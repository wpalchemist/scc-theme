<?php get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<nav id="breadcrumbs">
		<li>
			<a href="<?php echo home_url(); ?>/your-profile">Your Profile</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/your-profile/master-schedule">Master Schedule</a>
		</li>
		<li>
			Shift Details
		</li>
	</nav>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<header>
				<h1><?php the_title(); ?></h1>
			</header>

			<?php the_content(); ?>

		</article>

	<?php endwhile; // end of the loop. ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>