<?php get_header(); ?>


<?php if(is_user_logged_in() && (wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator'))) { ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<nav id="breadcrumbs">
		<li>
			<a href="<?php echo home_url(); ?>/your-profile">Your Profile</a>
		</li>
		<li>
			<a href="<?php echo home_url(); ?>/your-profile/?tab=clients">Clients</a>
		</li>
		<li>
			<?php the_title(); ?>
		</li>
	</nav>
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<header>
				<h1><?php the_title(); ?></h1>
			</header>

			<?php get_template_part('content', 'client'); ?>

		</article>

	<?php endwhile; // end of the loop. ?>

<?php } else { ?>
        <p class="warning">
            <?php _e('You must be logged in to view your profile.', 'wpaesm'); ?>
        </p><!-- .warning -->
        <?php echo do_shortcode('[wppb-login]'); ?>
        <p><a href="<?php echo home_url(); ?>/recover-password">Lost your password?</a></p>
<?php } ?>

</div><!-- #main -->
<?php get_footer(); ?>