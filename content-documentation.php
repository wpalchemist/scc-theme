<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Process the form, if it has been submitted
if( isset( $_POST['form_name'] ) && "documentation" == ( $_POST['form_name'] ) ) {
	if ( !wp_verify_nonce( $_POST['wpaesm_documentation_nonce'], "wpaesm_documentation_nonce")) {
        exit( "Permission error." );
    }
    // $error = "<pre>" . print_r($_POST, true) . "</pre>";
    // wp_die($error);
    $doclist = array();
	foreach( $_POST['doc'] as $doc ) {
		$employee = get_user_by( 'id', $doc['employeeid'] );

		// if we are updating an existing document
		if( isset( $doc['docid'] ) ) {
			$docid =  array(
			      'ID'           => $doc['docid'],
			      'post_content' => sanitize_text_field( $doc['notes'] ),
			);
			wp_update_post( $docid );
			$document = $doc['docid'];
			$doclist[] = $document;
		} else {
			// no existing document, so let's make a new one
			$docinfo = array(
					'post_type'     => 'document',
					'post_title'    => $employee->user_nicename  . '\'s shift with ' . $doc['clientname'] . ' on ' . $doc['date'],
					'post_status'   => 'publish',
					'post_content'	=> sanitize_text_field( $doc['notes'] ),
				);
			$document = wp_insert_post( $docinfo );
			$doclist[] = $document;
		}

		// add a serialised array for wpalchemy to work - see http://www.2scopedesign.co.uk/wpalchemy-and-front-end-posts/
		$fields = array('_wpaesm_ratings');
		$str = $fields;
		update_post_meta( $document, 'document_meta_fields', $str );
		 
		// Get all the goals and put them in the array format wpalchemy expects
		if( isset( $doc['goals'] ) ) {
			foreach ( $doc['goals'] as $goal ) {
				if( isset( $goal['rank'] ) )
				$goalfields[] = array(
					'goal' => $goal['name'],
					'score' => $goal['rank'],
					);
			}
		} 		 

		// Add all of the post meta data in one fell swoop
		if( isset( $goalfields ) ) {
			update_post_meta( $document, '_wpaesm_ratings', $goalfields );
		}

		// connect document to employee
		p2p_type( 'documents_to_employees' )->connect( $document, $doc['employeeid'], array(
		    'date' => current_time('mysql')
		) );
		// connect document to client
		p2p_type( 'documents_to_clients' )->connect( $document, $doc['clientid'], array(
		    'date' => current_time('mysql')
		) );
		// connect document to shift
		p2p_type( 'documents_to_shifts' )->connect( $document, $doc['shiftid'], array(
		    'date' => current_time('mysql')
		) );
		// give document "pending review" status
		$pending = get_term_by( 'slug', 'pending-review', 'doc_status' );
		wp_set_post_terms( $document, $pending->term_id, 'doc_status', 0 );

		if( $document ) { ?>
			<p class='success'>Documentation about your shift on <?php echo $doc['date']; ?> saved successfully.</p>
		<?php } else { ?>
			<p class='failure'>Sorry, there was an error saving documentation about your shift on <?php echo $doc['date']; ?></p>
		<?php }
		unset( $goalfields );
	}

	// send email notification to admins
	if( !empty( $doclist ) ) {
		$options = get_option('wpaesm_options');
		if(isset($options['admin_notification_email'])) {
			$to = $options['admin_notification_email'];
		} else {
			$to = get_bloginfo('admin_email');
		}
		$subject = __( 'New Documentation Notification', 'wpaesm' );

		$message = __( 'An employee has just submitted their documentation.  Details: ') . "\n\n";
		if( isset( $employee ) ) {
			$message .= __( 'Employee: ', 'wpaesm' ) . $employee->user_nicename . "\n";
		}
		$message .= __( 'Documents: ', 'wpaesm' ) . $employee->user_nicename . "\n";
		foreach( $doclist as $doc) {
			$message .= get_the_title( $doc ) . ": " . admin_url( 'post.php?post='.$doc.'&action=edit' ) . "\n";
		}

		$headers = "From: " . $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";
		wp_mail( $to, $subject, $message, $headers );
	}

}


// get the range of dates for this week
if(isset($_GET['week'])) {
	$thisweek = $_GET['week'];
	$nextweek = strtotime("+1 week", $thisweek);
	$lastweek = strtotime("-1 week", $thisweek);
} else {
	$thisweek = current_time("timestamp");
	$nextweek = strtotime("+1 week");
	$lastweek = strtotime("-1 week");
}

$options = get_option('wpaesm_options');

// find out what day of the week today is
$today = date("l", $thisweek);

if($today == $options['week_starts_on']) { // today is first day of the week
	$weekstart = $thisweek;
} else { // find the most recent first day of the week
	$sunday = 'last ' . $options['week_starts_on'];
	$weekstart = strtotime($sunday, $thisweek);
}

// from the first day of the week, add one day 7 times to get all the days of the week
$i = 0;
while($i < 7) {
	$week[date("Y-m-d", strtotime('+ ' . $i . 'days', $weekstart))] = array();
	if($i == 0) {
		$schedulebegin = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
	} elseif ($i == 6) {
		$scheduleend = date('F j, Y', strtotime('+ ' . $i . 'days', $weekstart));
	}
	$i++;
} ?>

<h3>Documentation for <?php echo $schedulebegin; ?> through <?php echo $scheduleend; ?></h3>
<nav class='schedule'>
	<li class='previous'>
		<a href='<?php echo home_url(); ?>/your-profile/?tab=documentation&week=<?php echo $lastweek; ?>'>Previous Week</a>
	</li>
	<li class='this'>
		<a href='<?php echo home_url(); ?>/your-profile/?tab=documentation'>This Week</a>
	</li>
	<li class='next'>
		<a href='<?php echo home_url(); ?>/your-profile/?tab=documentation&week=<?php echo $nextweek; ?>'>Next Week</a>
	</li>
</nav>

<form method='post' action='<?php get_the_permalink() ?>?tab=documentation<?php if(isset($_GET['week'])) { echo "&week=" . $_GET['week']; } ?>' id='documentation'>
	<input type='hidden' name='form_name' value='documentation'>
	<input name='wpaesm_documentation_nonce' id='wpaesm_documentation_nonce' type='hidden' value='<?php echo wp_create_nonce( 'wpaesm_documentation_nonce' ); ?>'>
	<?php
	// for each day of the week, get all of this employee's shifts for that day
	$i = 1;
	foreach($week as $day => $shifts) {
		$args = array( 
			'post_type' => 'shift',
			'meta_key' => '_wpaesm_date',
			'meta_value' => $day,
			'connected_type' => 'shifts_to_employees',
			'connected_items' => get_current_user_id(),
			'tax_query' => array(
				array(
					'taxonomy' => 'shift_type',
					'field'    => 'slug',
					'terms'    => array( 'extra', 'pto' ),
					'operator' => 'NOT IN',
				),
			),
		);
	
	$docquery = new WP_Query( $args );
	if ( $docquery->have_posts() ) : ?>
		<?php while ( $docquery->have_posts() ) : $docquery->the_post(); ?>
			<fieldset>
				<?php 
				$shiftid = get_the_id();
				global $shift_metabox; // get metabox data
				$shiftmeta = $shift_metabox->the_meta(); 
				// get client associated with this shift
				$clients = new WP_Query( array(
				  'connected_type' => 'shifts_to_clients',
				  'connected_items' => get_the_id(),
				  'nopaging' => true,
				) );
				$clientname = 'No client';
				if ( $clients->have_posts() ) :
					while ( $clients->have_posts() ) : $clients->the_post(); 
						$clientid = get_the_id();
					    $clientname = get_the_title();
					    global $client_details;
					    $clientmeta = $client_details->the_meta();
					    if( isset( $clientmeta['goal'] ) ) {
					    	$goals = $clientmeta['goal']; 
					    }
					endwhile; 
					wp_reset_postdata();
				endif;
				// query for documents associated with this shift
				$old_docs = new WP_Query( array(
				  'connected_type' => 'documents_to_shifts',
				  'connected_items' => $shiftid,
				  'nopaging' => true,
				) );
				if ( $old_docs->have_posts() ) :
					while ( $old_docs->have_posts() ) : $old_docs->the_post(); 
						$docid = get_the_id();
					    global $document_metabox;
					    $docmeta = $document_metabox->the_meta();
					    if( isset( $docmeta['ratings'] ) ) {
						    $ratings = $docmeta['ratings']; 
						}
					    $docnotes = get_the_content();
					endwhile; 
					wp_reset_postdata();
				endif;
				// if there are documents, pre-fill the form with their values
				?>
				<legend>Documentation for shift with <?php echo $clientname; ?> on <?php echo date("D M j", strtotime($shiftmeta['date'])); ?></legend>
				<input type="hidden" name="doc[doc<?php echo $i; ?>][clientid]" value="<?php echo $clientid; ?>">
				<input type="hidden" name="doc[doc<?php echo $i; ?>][clientname]" value="<?php echo $clientname; ?>">
				<input type="hidden" name="doc[doc<?php echo $i; ?>][employeeid]" value="<?php echo get_current_user_id(); ?>">
				<input type="hidden" name="doc[doc<?php echo $i; ?>][shiftid]" value="<?php echo $shiftid; ?>">
				<input type="hidden" name="doc[doc<?php echo $i; ?>][date]" value="<?php echo $shiftmeta['date']; ?>">
				<?php if( isset ( $docid ) ) { ?>
					<input type="hidden" name="doc[doc<?php echo $i; ?>][docid]" value="<?php echo $docid; ?>">
				<?php } ?>
				<label><strong>Notes: RIPE format and narrative</strong></label>
				<textarea name="doc[doc<?php echo $i; ?>][notes]" id="notes"><?php if( isset( $docnotes ) ) { echo $docnotes; } ?>
				</textarea>
				<label><strong>Behavior Ratings</strong></label>
				<?php $j = 1; ?>
				<?php if( isset( $goals ) ) {
					foreach( $goals as $goal ) {
						$oldscore = array();
						if( isset( $goal['active'] ) && $goal['active'] == 'active' ) {
							// check existing ratings to see if we need to pre-populate the form
							if( !empty( $ratings ) ) {
								foreach( $ratings as $rating ) {
									if( $rating['goal'] == $goal['goal_name'] ) {
										// create an array of the values so we can stick them in where needed
										$oldscore[$rating['score']] = true;
									}
								} 
							}?>
									<p>
										<label class="goal"><?php echo $goal['goal_name']; ?></label>
										<input type="hidden" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][name]" value="<?php echo $goal['goal_name']; ?>">
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[0]) ? ' checked' : '')?> value="0">NA</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[1]) ? ' checked' : '')?> value="1">1</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[2]) ? ' checked' : '')?> value="2">2</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[3]) ? ' checked' : '')?> value="3">3</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[4]) ? ' checked' : '')?> value="4">4</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[5]) ? ' checked' : '')?> value="5">5</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[6]) ? ' checked' : '')?> value="6">6</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[7]) ? ' checked' : '')?> value="7">7</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[8]) ? ' checked' : '')?> value="8">8</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[9]) ? ' checked' : '')?> value="9">9</input>
										<input type="radio" name="doc[doc<?php echo $i; ?>][goals][goal<?php echo $j; ?>][rank]" <?php echo (isset($oldscore[10]) ? ' checked' : '')?> value="10">10</input>
									</p>
							<?php $j++;
								}
							
							unset($oldscore);
						}
						
				} else { ?>
					<p>No behavior goals have been set for this client.</p>
				<?php } 
				unset( $goals ); ?>
			</fieldset>
			
		<?php endwhile; ?>
	<?php endif;
	wp_reset_postdata();
	$i++; 
}  ?>
<input type="submit" value="Save Documentation">
</form>