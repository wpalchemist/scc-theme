<?php
/**
 * Template Name: Login Required
 *
 */

get_header(); ?>

<?php if(is_user_logged_in() && (wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator'))) { ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php if ( is_page() && $post->post_parent > 0 ) { ?>
		    <nav id="breadcrumbs">
				<li>
					<a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>
				</li>
				<li>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</li>
			</nav>
		<?php } ?>



		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
					<h1><?php the_title(); ?></h1>
			</header>				

			<?php the_content(); ?>

		</article>

	<?php endwhile; ?>

<?php } else { ?>
        <p class="warning">
            <?php _e('You must be logged in to view your profile.', 'wpaesm'); ?>
        </p><!-- .warning -->
        <?php echo do_shortcode('[wppb-login]'); ?>
        <p><a href="<?php echo home_url(); ?>/recover-password">Lost your password?</a></p>
<?php } ?>

</div><!-- #main -->



<?php get_footer(); ?>