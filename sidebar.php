</div><!-- #main -->
	<aside>

        <?php if ( is_active_sidebar( 'wpa-sidebar' ) ) {
            dynamic_sidebar( 'wpa-sidebar' ); 
        } ?>

        <div class="seeking">
        	<h3>Seeking Specialists</h3>
        	<p>Interested in joining us? We are always seeking qualified Behavior Specialists. <a href="<?php echo home_url(); ?>/contact">Learn More ></a></p>
		</div>

		<div class="we-are">
			<?php if ( is_active_sidebar( 'wpa-we-are' ) ) {
                            dynamic_sidebar( 'wpa-we-are' ); 
                        } ?>
    	</div>

	</aside>