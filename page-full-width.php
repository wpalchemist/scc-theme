<?php
/**
 * Template Name: Full Width, No Sidebar
 *
 */

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php if ( is_page() && $post->post_parent > 0 ) { ?>
		    <nav id="breadcrumbs">
				<li>
					<a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>
				</li>
				<li>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</li>
			</nav>
		<?php } ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
					<h1><?php the_title(); ?></h1>
			</header>				
			<?php the_content(); ?>
		</article>

	<?php endwhile; ?>

</div><!-- #main -->

<?php get_footer(); ?>