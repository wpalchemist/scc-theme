<?php 
// Client details
?>
<p class="editable instructions">Double-click on any of the grey areas to edit them.</p>
<p class="edit-link"><a href="<?php the_permalink(); ?>">Edit <?php the_title(); ?>'s details</a></p>
<div class="contact">
	<?php if( has_post_thumbnail() ) {
		the_post_thumbnail( 'medium', array( 'class' => 'alignright') );
	} ?>

	<?php global $client_metabox;
    $meta = $client_metabox->the_meta(); ?>

    <ul class="contactinfo">
    	<?php if( isset ( $meta['school'] ) ) { ?>
	    	<li><strong>School:</strong> <?php echo $meta['school']; ?></li>
	    <?php } ?>
    	
    	<?php if( isset( $meta['clientphone'] ) ) {
	    	$phones = $meta['clientphone'];
	    	foreach( $phones as $phone ) { ?>
	    		<li>
	    			<?php echo $phone['phonenumber']; ?>
	    			<?php if( isset($phone['phonetype']) ) {
		    			echo "( " . $phone['phonetype'] . " )";
		    		} ?> 
	    		</li>
	    	<?php } 
	    }?>
    	<?php if( isset( $meta['clientemail'] ) ) {
	    	$emails = $meta['clientemail'];
	    	foreach( $emails as $email ) { ?>
	    		<li><a href="mailto:<?php echo $email['emailaddress']; ?>"><?php echo $email['emailaddress']; ?></a>
	    			<?php if( isset($email['emailtype']) ) {
		    			echo "( " . $email['emailtype'] . " )";
		    		} ?>
	    		</li>
	    	<?php } 
	    }?>
    	<?php if( isset( $meta['address'] ) ) {
	    	$addresses = $meta['address'];
	    	foreach( $addresses as $address ) { ?>
	    		<li><strong><?php if( isset($address['addressname']) ) {
	    			echo $address['addressname'];
	    		} ?> Address: </strong><br />
	    		<?php echo $address['address']; ?><br />
	    		<?php echo $address['city'] . ", " . $address['state'] . " " . $address['zip']; ?>
	    		</li>
	    	<?php } 
	    } ?>
    </ul>

    <?php if( isset( $meta['parent'] ) ) { 
    	$parents = $meta['parent']; ?>
    	<h3>Parent(s)/Guardian(s)</h3>
    	<div class="parents clearfix">
    		<?php foreach( $parents as $parent ) { ?>
    		<ul>
	    		<li><strong><?php echo $parent['name']; ?></strong></li>
	    		<li><?php echo $parent['relation']; ?></li>
	    		<?php if( isset($parent['phone']) ) {
		    		$phones = $parent['phone'];
			    	foreach( $phones as $phone ) { ?>
			    		<li>
			    			<?php echo $phone['phonenumber']; ?> 
			    			<?php if( isset( $phone['phonetype'] ) ) {
				    			echo "(" . $phone['phonetype'] . ")";
				    		} ?>
			    		</li>
			    	<?php } 
			    } ?>
		    	<?php if( isset($parent['email']) ) {
			    	$emails = $parent['email'];
			    	foreach( $emails as $email ) { ?>
			    		<li>
			    			<a href="mailto:<?php echo $email['emailaddress']; ?>">
			    				<?php echo $email['emailaddress']; ?></a> 
			    				<?php if ( isset( $email['emailtype'] ) ) {
				    				echo "(" . $email['emailtype'] . ")";
				    			} ?>
			    		</li>
			    	<?php } 
			    }?>
			</ul>
    		<?php } ?>
    	</div>
    <?php } ?>

</div>

<div class="clientinfo">

	<?php     
	global $client_details;
    $details = $client_details->the_meta();
    ?>

	<h3>About <?php the_title(); ?></h3>
	<div class=" editable editable-description" id="description">
		<?php the_content(); ?>
	</div>

	<?php if( isset( $details['diagnosis'] ) ) { ?>
		<h3>Diagnosis</h3>
		<div class="editable editable-diagnosis" id="diagnosis">
			<?php echo nl2br( $details['diagnosis'] ); ?>
		</div>
	<?php } ?>

	<?php if( isset( $details['triggers'] ) ) { ?>
		<h3>Triggers</h3>
		<div class="editable editable-triggers">
			<?php echo nl2br( $details['triggers'] ); ?>
		</div>
	<?php } ?>

	<?php if( isset( $details['activities'] ) ) { ?>
		<h3>Activities</h3>
		<div class="editable editable-activities">
			<?php echo nl2br( $details['activities'] ); ?>
		</div>
	<?php } ?>

	<?php if( isset( $details['restraint'] ) ) { ?>
		<h3>Restraint</h3>
		<p>Restraints approved in IEP</p>
		<?php if( isset( $details['restraint_details'] ) ) { ?>
			<div class="restraint-details">
				<?php echo nl2br( $details['restraint_details'] ); ?>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if( isset($details['goal']) ) { ?>
		<h3>Current Behavior Goals</h3>
		<ul class="goals">
			<?php $goals = $details['goal'];
			foreach( $goals as $goal ) {
				if(isset($goal['active']) && $goal['active'] == 'active') { ?>
					<li><?php echo $goal['goal_name']; ?></li>
				<?php }
			} ?>
		</ul>
	<?php } ?>

	<?php if( isset( $details['sub'] ) ) { ?>
		<h3>Sub Plan</h3>
		<a id="subTag">
			Show Sub Plan
		</a>
		<div id="sub" style="display: none;">
			<div class="editable editable-sub">
				<?php echo $details['sub']; ?>
			</div>
		</div>
	<?php } ?>

	<?php if( isset( $details['crisis'] ) ) { ?>
		<h3>Crisis Plan</h3>
		<a id="crisisTag">
			Show Crisis Plan
		</a>
		<div id="crisis" style="display: none;">
			<div class="editable editable-crisis">
				<?php echo $details['crisis']; ?>
			</div>
		</div>
	<?php } ?>

	<?php if( is_singular( 'client' ) ) {
		// Find connected documents
		$docs = new WP_Query( array(
		  'connected_type' => 'documents_to_clients',
		  'connected_items' => get_the_id(),
		  'posts_per_page' => 25,   
		) );

		if ( $docs->have_posts() ) : ?>
			<h3>Documents</h3>
			<div id="docs">
				<?php while ( $docs->have_posts() ) : $docs->the_post(); ?>
					<div class="document">
				    	<?php 
				    	$docid = get_the_id();
				    	$content = get_the_content();
				    	global $document_metabox;
					    $docmeta = $document_metabox->the_meta();
					    if( isset( $docmeta['ratings'] ) ) {
					    	$ratings = $docmeta['ratings'];
					    } ?>
					    <p><strong>Shift Date: </strong>
					    	<?php // Find connected shifts
								$shifts = new WP_Query( array(
								  'connected_type' => 'documents_to_shifts',
								  'connected_items' => $docid,
								) );
								if( $shifts->have_posts() ) {
									while ( $shifts->have_posts() ) : $shifts->the_post();
										echo get_post_meta( get_the_id(), '_wpaesm_date', true );
									endwhile;
								}
								wp_reset_postdata();
							?>
					    <br />
					    <strong>Employee: </strong>
					    	<?php // Find connected employee
								$employees = get_users( array(
								  'connected_type' => 'documents_to_employees',
								  'connected_items' => $docid
								) );
								if( !empty( $employees ) ) {
									foreach( $employees as $employee ) {
										echo $employee->display_name;
									}
								}
							?>
						</p>
					    <div>
					    	<strong>Notes:</strong><br /> <?php echo $content; ?>
					    </div>
					    <?php if( !empty( $ratings ) ) { ?>
					    	<p><strong>Behavior Goals: </strong>
					    		<ul>
					    			<?php foreach( $ratings as $rating ) { ?>
					    				<li><?php echo $rating['goal'] . ': ' . $rating['score']; ?></li>
					    			<?php } ?>
					    		</ul>
					    	</p>
					    <?php } ?>
					</div>
				<?php endwhile;
				
				wp_reset_postdata(); ?>

			</div>
		<?php endif; 
	}?>
</div>

<input type="hidden" id="clientid" value="<?php the_id(); ?>">
<input name="wpascc_client_nonce" id="wpascc_client_nonce" type="hidden" value="<?php echo wp_create_nonce( 'wpascc_client_nonce' ); ?>">