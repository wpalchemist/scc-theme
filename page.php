<?php
/**
 * The default template for displaying all pages.
 *
 */

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php if ( is_page() && $post->post_parent > 0 ) { ?>
			    <nav id="breadcrumbs">
					<li>
						<a href="<?php echo get_permalink($post->post_parent); ?>"><?php echo get_the_title( $post->post_parent ); ?></a>
					</li>
					<li>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				</nav>
			<?php } ?>

			<header>
					<h1><?php the_title(); ?></h1>
			</header>		

			<div class="featuredimg">
				<?php $images = array( 'header_1.jpg', 'header_2.jpg','header_3.jpg', 'header_4.jpg', 'header_5.jpg' );
				$img =  get_stylesheet_directory_uri() . '/images/headers/' . $images[array_rand( $images )]; ?>
				<img src="<?php echo $img; ?>">
			</div>		

				<?php the_content(); ?>

		</article>

	<?php endwhile; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
