<h3>Generate and Approve Timesheet</h3>

<?php 
// get array of time period begin dates
$beginnings = array();
$date = new DateTime("2015-01-01");
$beginnings[] = $date->format("Y-m-d");
$date->modify("+15 days");
$beginnings[] = $date->format("Y-m-d");
$now = new DateTime('NOW');

while ( $date < $now ) {
    $date->modify( "first day of next month" );
    $beginnings[] = $date->format( "Y-m-d" );
    $date->modify( "+15 days" );
    $beginnings[] = $date->format( "Y-m-d" );
}

$beginnings = array_reverse( $beginnings );
?>

<form method='post' action='<?php the_permalink(); ?>?tab=timesheet' id='view-timesheet'>
	<p><label>View timesheet beginning on: </label>
        <select name="beginning">
            <?php foreach( $beginnings as $beginning ) { ?>
                <option value="<?php echo $beginning; ?>"><?php echo $beginning; ?></option>
            <?php } ?>
        </select>
	</p>
	<p class="submit">
	<input type="submit" class="button-primary" value="<?php _e( 'View Timesheet', 'wpaesm' ); ?>" />
	</p>
</form>

<?php if( isset( $_POST['beginning'] ) ) { 
	$start = strtotime( $_POST['beginning'] );

	// find the end of the pay period
	if( "1" == date( 'd', $start ) ) {
		// $end = $start->modify("+14 days");
		$end = date( 'Y-m-d', strtotime( '+14 days', $start ) );
	} elseif( "16" == date( 'd', $start ) ) {
		// $end = $start->modify("first day of next month");
		$end = date( 'Y-m-d', strtotime( 'last day of this month', $start ) );
	}

	// find the employee
	$employee = get_current_user_id();

	// find out if we are viewing the previous pay period
	$today = current_time( "d" );
	if( "16" > $today ) {
		// we are in the first half of the month, so the previous time period began on the 16th of last month
		$previousmonth = strtotime( 'first day of last month', time() );
		$previous = date( 'Y-m-d', strtotime( '+ 15 days', $previousmonth ) );
	} else {
		// we are in the second half of the month, so the previous time period began on the 1st of this month
		$previous = date( 'Y-m-d', strtotime( 'first day of this month', time() ) );
	} ?>

	<p><a class="button-primary" id="show-disapprove">Request Changes To This Timesheet</a></p>
		<form method='post' action='<?php the_permalink(); ?>?tab=timesheet' id='disapprove-timesheet' style="display: none;">
			<label>Why do you not approve this timesheet?  What changes need to be made?</label>
			<textarea id="reasons"></textarea>
			<input type="hidden" id="employee" value="<?php echo $employee; ?>">
			<input type="hidden" id="start" value="<?php echo $_POST['beginning']; ?>">
			<input type="hidden" id="end" value="<?php echo $end; ?>">
			<input type="hidden" id="disapprove_timesheet_nonce" value="<?php echo wp_create_nonce( 'disapprove_timesheet_nonce' ); ?>">
			<p><a class="button-primary" id="disapprove">Send Request</a></p>
		</form>
	<?php if( $previous == $_POST['beginning'] ) {
		// we are viewing the previous timesheet, so we might need an 'approve' button
        // approval timeframe starts on the last day and 15th day of the month at 7pm, ends after 3 days
		date_default_timezone_set( get_option('timezone_string') );
	    $first_approval_timeframe_start = strtotime( '7pm last day of this month', time() );
	    $this_month = strtotime( '7pm first day of this month', time() );
	    $second_approval_timeframe_start = strtotime( '+ 14 days', $this_month );
	    $third_approval_timeframe_start = strtotime( '7pm last day of last month', time() );

	    if(
            $first_approval_timeframe_start <= time() && time() <= ( $first_approval_timeframe_start + 259200 ) ||
	        $second_approval_timeframe_start <= time() && time() <= ( $second_approval_timeframe_start + 259200 ) ||
            $third_approval_timeframe_start <= time() && time() <= ( $third_approval_timeframe_start + 259200 )
        ) { ?>

            <form method='post' action='<?php the_permalink(); ?>?tab=timesheet' id='approve-timesheet'>
                <input type="hidden" id="employee" value="<?php echo $employee; ?>">
                <input type="hidden" id="start" value="<?php echo $_POST['beginning']; ?>">
                <input type="hidden" id="end" value="<?php echo $end; ?>">
                <input type="hidden" id="approve_timesheet_nonce" value="<?php echo wp_create_nonce( 'approve_timesheet_nonce' ); ?>">
                <p><a class="button-primary" id="approve">Approve This Timesheet</a></p>
            </form>

        <?php }
	    } ?>

	<div id="approval-message"></div>

	<?php // generate and display the timesheet
	wpa_generate_employee_timesheet( $employee, $_POST['beginning'], $end );
} ?>

