<?php
/**
 * Template Name: Profile
 *
 */


if ( ! defined( 'ABSPATH' ) ) exit;

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header>
					<h1><?php the_title(); ?></h1>
			</header>		

            <?php if(is_user_logged_in() && (wpaesm_check_user_role('employee') || wpaesm_check_user_role('administrator'))) { ?>

				<?php the_content(); ?>

                <div id="employee-info">
                    <ul id="nav" class="nav">
                        <li><a href="#schedule" class="current">Schedule</a></li>
                        <li><a href="#clients">Clients</a></li>
                        <li><a href="#documentation">Documentation</a></li>
                        <li><a href="#extra-shift">Record Extra Work</a></li>
                        <li><a href="#expense">Expenses</a></li>
                        <li><a href="#forms">Forms &amp; Links</a></li>
                        <li><a href="#timesheet">Timesheet</a></li>
                    </ul>
                    
                    <div class="list-wrap">
                        <ul id="schedule">
                            <?php echo do_shortcode('[your_schedule]'); ?>
                            <p><a href="<?php echo home_url(); ?>/master-schedule">View Master Schedule</a></p>
                        </ul>
                        
                        <ul id="clients" class="hide">
                            <h2>This Week's Clients</h2>
                            <?php 
                            if( is_array( $clientarray ) ) {
                                foreach( $clientarray as $client ) {
                                    $args = array( 
                                        'post_type' => 'client', 
                                        'post_title_like' => $client,
                                        'posts_per_page' => -1, 
                                        'order' => 'ASC',
                                        'orderby' => 'title',
                                    );
                                    
                                    $myclients = new WP_Query( $args );
                                    
                                    // The Loop
                                    if ( $myclients->have_posts() ) :
                                        while ( $myclients->have_posts() ) : $myclients->the_post(); ?>
                                            <div class="myclient">
                                                <h1><?php the_title(); ?></h1>
                                                <?php get_template_part('content', 'client'); ?>
                                            </div>
                                            
                                        <?php endwhile;
                                    endif;
                                    
                                    // Reset Post Data
                                    wp_reset_postdata();
                                }
                            } else {
                                echo "No clients found.";
                            }


                            ?>

                            <h2>All Clients</h2>
                            <div class="all-clients clearfix">
                                <?php $client_categories = get_terms( 'client_category', 'exclude=18' );
                                foreach( $client_categories as $category ) { ?>
                                    <div class="client-group clearfix">
                                        <h3><?php echo $category->name; ?></h3>
                                        <ul>
                                            <?php echo wpa_display_client_category( $category->slug ); ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </ul>
                        
                        <ul id="documentation" class="hide">
                            <p>
                                <a href="<?php echo home_url(); ?>/your-profile/unapproved-documents">Your Unapproved Documents</a> | 
                                <a href="<?php echo home_url(); ?>/your-profile/approved-documents">Your Approved Documents</a>
                            </p>
                            <?php echo do_shortcode('[documents_to_do]'); ?>
                        </ul>
                            
                        <ul id="extra-shift" class="hide">
                            <?php echo do_shortcode('[extra_work]'); ?>
                        </ul>

                        <ul id="expense" class="hide">
                            <?php echo do_shortcode('[record_expense]'); ?>
                        </ul>

                        <ul id="forms" class="hide">

                            <?php $args = array( 
                                'pagename' => 'your-profile/forms-and-links',
                            );
                            
                            $formspage = new WP_Query( $args );
                            
                            // The Loop
                            if ( $formspage->have_posts() ) :
                                while ( $formspage->have_posts() ) : $formspage->the_post();
                                    the_content();
                                endwhile;
                            endif;
                            
                            // Reset Post Data
                            wp_reset_postdata(); ?>
                            
                        </ul>

                        <ul id="timesheet" class="hide">
                            <?php get_template_part('content', 'timesheet'); ?>
                        </ul>
                    </div> <!-- END List Wrap -->

                </div>



            <?php } else { ?>
                <p class="warning">
                    <?php _e('You must be logged in to view your profile.', 'wpaesm'); ?>
                </p><!-- .warning -->
                <?php echo do_shortcode('[wppb-login]'); ?>
                <p><a href="<?php echo home_url(); ?>/recover-password">Lost your password?</a></p>
            <?php } ?>

		</article>

	<?php endwhile; ?>

</div><!-- #main -->
<?php get_footer();

function wpa_display_client_category( $category ) {
    $these_clients = '';

    $args = array( 
        'post_type' => 'client', 
        'posts_per_page' => -1, 
        'order' => 'ASC',
        'orderby' => 'title',
        'client_category' => $category,
    );
    
    $clients = new WP_Query( $args );
    
    // The Loop
    if ( $clients->have_posts() ) :
        while ( $clients->have_posts() ) : $clients->the_post();
            $these_clients .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
        endwhile;
    endif;
    
    // Reset Post Data
    wp_reset_postdata();

    return $these_clients;

} ?>