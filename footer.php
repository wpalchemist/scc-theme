	</div><!-- #content -->
	<footer id="bottom" class="clearfix">

		<p class="logo"><a href="<?php echo home_url(); ?>/your-profile">Seattle Community Care</a></p>

		<p class="address">Community Care: 5424 Delridge Way SW Suite A, Seattle, WA 98106 | Phone: (206) 937-4217  | Fax: (206) 937-6176<br />
		Content and all forms copyright protected, 2015. Community Care, LLC. All rights reserved. </p>

	</footer>
</div><!-- #wrapper -->
<?php wp_footer(); ?>
</body>
</html>