<?php
/**
 * The default template for displaying all pages.
 *
 */

get_header(); ?>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="welcome">
				<?php the_content(); ?>

				<?php if ( is_active_sidebar( 'wpa-homepage-quote' ) ) { ?>
					<div class="quote">
		                <?php dynamic_sidebar( 'wpa-homepage-quote' ); ?>
		            </div>
		        <?php } ?>
			</div>

			<div class="we-are">
				<?php if ( is_active_sidebar( 'wpa-we-are' ) ) {
					dynamic_sidebar( 'wpa-we-are' );
				} ?>
			</div>

			<div id="columns" class="clearfix">
		        <?php if ( is_active_sidebar( 'wpa-homepage' ) ) {
		            dynamic_sidebar( 'wpa-homepage' ); 
		        } ?>

			</div>

		</article>

	<?php endwhile; ?>

</div><!-- #main -->

<?php get_footer(); ?>
