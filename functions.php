<?php

define( 'scc_VERSION', '1.2.1' );

/** Tell WordPress to run wpa_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', 'wpa_setup' );

if ( ! function_exists( 'wpa_setup' ) ):

function wpa_setup() {
	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size('pagehead', 673, 182, true);

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
        
    // Add support for visual editor stylesheet
    add_editor_style('editor-style.css');

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'wpa', TEMPLATEPATH . '/languages' );

	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'wpa' ),
	) );
}
endif;

/** Set up scripts **/

function wpa_script_enqueue() {
	wp_enqueue_style( 'scc-style', get_stylesheet_uri(), array(), scc_VERSION );

	if ( is_singular() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    wp_enqueue_script('jquery');
    if( is_page( 'your-profile' ) ) {
    	wp_enqueue_script('idTabs', get_bloginfo('template_directory') . '/js/jquery.idTabs.min.js', array('jquery') );
    	wp_enqueue_script('theme_scripts', get_bloginfo('template_directory') . '/js/wpascript.js', array('jquery') );
    	wp_localize_script( 'theme_scripts', 'sccthemeajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    }
    if( is_page( 'master-schedule') || is_page( 'master-schedule-2') ) {
    	wp_enqueue_script('theme_scripts', get_bloginfo('template_directory') . '/js/wpascript.js', array('jquery') );
    }
    if( is_singular( 'client' ) ) {
    	wp_enqueue_script('ipweditor', get_bloginfo('template_directory') . '/js/jquery.ipweditor-1.2.1.js', array('jquery') );
    	wp_enqueue_script('tinymce', get_bloginfo('template_directory') . '/js/tiny_mce_src.js', array('jquery', 'ipweditor') );
    	wp_enqueue_script('edit_client', get_bloginfo('template_directory') . '/js/edit_client.js', array('jquery') );
	    wp_localize_script( 'edit_client', 'editclientajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	    wp_enqueue_script('theme_scripts', get_bloginfo('template_directory') . '/js/wpascript.js', array('jquery') );
    }
}
add_action('wp_enqueue_scripts', 'wpa_script_enqueue', 11);

/**
 * Set our wp_nav_menu() fallback, wpa_menu().
 */
function wpa_menu() {
	echo '<nav><ul><li><a href="'.get_bloginfo('url').'">Home</a></li>';
	wp_list_pages('title_li=');
	echo '</ul></nav>';
}

/**
 * Remove inline styles printed when the gallery shortcode is used.
 */
add_filter( 'use_default_gallery_style', '__return_false' );

/**
 * @return string The gallery style filter, with the styles themselves removed.
 */
function wpa_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}

if ( ! function_exists( 'wpa_comment' ) ) :
/**
 * Template for comments and pingbacks.
 */
function wpa_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<article <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s says:', 'wpa' ), sprintf( '%s', get_comment_author_link() ) ); ?>
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<?php _e( 'Your comment is awaiting moderation.', 'wpa' ); ?>
			<br />
		<?php endif; ?>

		<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'wpa' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'wpa' ), ' ' );
			?>

		<?php comment_text(); ?>

			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<article <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
		<p><?php _e( 'Pingback:', 'wpa' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'wpa'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/**
 * Closes comments and pingbacks with </article> instead of </li>.
 */
function wpa_comment_close() {
	echo '</article>';
}

/**
 * Adjusts the comment_form() input types for HTML5.
 */
function wpa_fields($fields) {
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
$fields =  array(
	'author' => '<p><label for="author">' . __( 'Name', 'wpa' ) . '</label> ' . ( $req ? '*' : '' ) .
	'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
	'email'  => '<p><label for="email">' . __( 'Email', 'wpa' ) . '</label> ' . ( $req ? '*' : '' ) .
	'<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
	'url'    => '<p><label for="url">' . __( 'Website', 'wpa' ) . '</label>' .
	'<input id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p>',
);
return $fields;
}
add_filter('comment_form_default_fields','wpa_fields');

/**
 * Register widgetized areas.
 */

function wpa_widgets_init() {
  $sidebars = array('Sidebar', 'Homepage', 'Homepage Quote', 'We Are');

  foreach($sidebars as $sidebar) {
    register_sidebar(
      array(
        'id'            => 'wpa-' . sanitize_title($sidebar),
        'name'          => __($sidebar, 'wpa'),
        'description'   => __($sidebar, 'wpa'),
		'before_widget' => '<div class="%2$s widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
      )
    );
  }
}

/** Register sidebars by running wpa_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'wpa_widgets_init' );

if ( ! function_exists( 'wpa_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post—date/time and author.
 */
function wpa_posted_on() {
	printf( __( 'Posted on %2$s by %3$s', 'wpa' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time datetime="%3$s" pubdate>%4$s</time></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date('Y-m-d'),
			get_the_date()
		),
		sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'wpa' ), get_the_author() ),
			get_the_author()
		)
	);
}
endif;


if ( ! function_exists( 'wpa_posted_in' ) ) {
	/**
	 * Prints HTML with meta information for the current post (category, tags and permalink).
	 */
	function wpa_posted_in() {
		// Retrieves tag list of current post, separated by commas.
		$tag_list = get_the_tag_list( '', ', ' );
		if ( $tag_list ) {
			$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'wpa' );
		} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
			$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'wpa' );
		} else {
			$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'wpa' );
		}
		// Prints the string, replacing the placeholders.
		printf(
			$posted_in,
			get_the_category_list( ', ' ),
			$tag_list,
			get_permalink(),
			the_title_attribute( 'echo=0' )
		);
	}
}

/**
 * Replacing the default WordPress search form with an HTML5 version
 *
 */
function html5_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <label class="assistive-text" for="s">' . __('Search for:', 'wpa' ) . '</label>
    <input type="search" placeholder="'.__("Search").'" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'html5_search_form' );

/**
* Prevent theme updates
**/
function wpa_prevent_theme_update( $r, $url ) {
    if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
        return $r; // Not a theme update request. Bail immediately.
    $themes = unserialize( $r['body']['themes'] );
    unset( $themes[ get_option( 'template' ) ] );
    unset( $themes[ get_option( 'stylesheet' ) ] );
    $r['body']['themes'] = serialize( $themes );
    return $r;
}
 
add_filter( 'http_request_args', 'wpa_prevent_theme_update', 5, 2 );


// ------------------------------------------------------------------------
// FILTER SO WE CAN SEARCH BY TITLE
// http://wordpress.stackexchange.com/questions/18703/wp-query-with-post-title-like-something
// ------------------------------------------------------------------------

function wpaesm_search_by_title( $where, &$wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( like_escape( $post_title_like ) ) . '%\'';
    }
    return $where;
}

add_filter( 'posts_where', 'wpaesm_search_by_title', 10, 2 );


/**
* AJAX functions for front end editing
**/

function wpascc_save_client_description() {

	// $results = "<pre>" . print_r($_POST, true) . "</pre>";
	// die($results);
	
    if ( !wp_verify_nonce( $_POST['nonce'], "wpascc_client_nonce")) {
        exit( "Permission error." );
    }

    $id = $_POST['clientid'];

    // update client description
 	if( isset( $_POST['description'] ) ) {
	    $newcontent = $_POST['description']['current'];
	    $update_client = array(
		  'ID'           => $id,
		  'post_content' => $newcontent
		);
		wp_update_post( $update_client );
		$results = "description updated";
	}

	if( isset( $_POST['diagnosis'] ) ) {
	    $newdiagnosis = $_POST['diagnosis']['current'];
	    update_post_meta( $id, '_wpaesm_diagnosis', $newdiagnosis );
		$results = "diagnosis updated";
	}

	if( isset( $_POST['triggers'] ) ) {
	    $newtriggers = $_POST['triggers']['current'];
	    update_post_meta( $id, '_wpaesm_triggers', $newtriggers );
		$results = "triggers updated";
	}

	if( isset( $_POST['activities'] ) ) {
	    $newactivities = $_POST['activities']['current'];
	    update_post_meta( $id, '_wpaesm_activities', $newactivities );
		$results = "activities updated";
	}

	if( isset( $_POST['sub'] ) ) {
	    $newsub = $_POST['sub']['current'];
	    update_post_meta( $id, '_wpaesm_sub', $newsub );
		$results = "sub plan updated";
	}

	if( isset( $_POST['crisis'] ) ) {
	    $newcrisis = $_POST['crisis']['current'];
	    update_post_meta( $id, '_wpaesm_crisis', $newcrisis );
		$results = "crisis plan updated";
	}

  
    die($results);
}
add_action( 'wp_ajax_nopriv_wpascc_save_client_description', 'wpascc_save_client_description' );
add_action( 'wp_ajax_wpascc_save_client_description', 'wpascc_save_client_description' );

// go to homepage after logging out

add_action('wp_logout','wpa_go_home');
function wpa_go_home(){
  wp_redirect( home_url() );
  exit();
}

// expire session after 1 hour
function wpaesm_cookie_expiration( $expiration, $user_id, $remember ) {
	if( user_can( $user_id, 'administrator' ) ) {
		return $remember ? $expiration : 14400;
	} else {
        return $remember ? $expiration : 3600;
    }
}
add_filter( 'auth_cookie_expiration', 'wpaesm_cookie_expiration', 99, 3 );

// Add more fields to shift
function wpa_add_fields_to_shift() { 
	global $shift_metabox; ?>
	<tr>
		<th scope="row"><label><?php _e('Break', 'wpaesm'); ?></label></th>
		<td>
			<?php $shift_metabox->the_field('break'); ?>
			<span><?php _e('Did employee take a break?', 'wpaesm'); ?></span><br />
			<input type="checkbox" name="<?php $shift_metabox->the_name(); ?>" value="1"<?php if ($shift_metabox->get_the_value()) echo ' checked="checked"'; ?>/><?php _e(' Break taken', 'wpaesm'); ?>
		</td>
	</tr>
<?php }
add_action( 'wpaesm_extra_shift_fields', 'wpa_add_fields_to_shift' );

// Create page for timesheets 

function wpa_add_options_page() {
	global $timesheet_page;
	$timesheet_page = add_submenu_page( '/employee-schedule-manager/options.php', 'Timesheets', 'Timesheets', 'manage_options', 'timesheet', 'wpa_create_timesheet_form' );
}	
add_action('admin_menu', 'wpa_add_options_page');

function wpa_enqueue_timesheet_script( $hook ) {
	global $timesheet_page;
	if ( $hook == $timesheet_page ) {
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
	}
}
add_action( 'admin_enqueue_scripts', 'wpa_enqueue_timesheet_script' );

// Create timesheets

function wpa_create_timesheet_form() { ?>
	<div class="wrap">

		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h1>Generate Employee Timesheets</h1>

		<h3>View One Employee's Timesheet</h3>

		<form method='post' action='<?php echo admin_url( 'admin.php?page=timesheet'); ?>' id='payroll-report'>
			<table class="form-table">
				<tr>
					<th scope="row">Employee:</th>
					<td>
						<select name="employee">
							<option value=""></option>
							<?php $employees = array_merge( get_users( 'role=employee&orderby=nicename' ), get_users( 'role=administrator&orderby=nicename' ) );
							usort( $employees, 'wpaesm_alphabetize' );
							foreach ( $employees as $employee ) { ?>
								<option value="<?php echo $employee->ID; ?>" ><?php echo $employee->display_name; ?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<th scope="row">Date Range:</th>
					<td>
						From <input type="text" size="10" name="thisdate" id="thisdate" value="" /> to <input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" name="submit" value="<?php _e( 'View Timesheet', 'wpaesm' ); ?>" />
			</p>
		</form>

		<h3>Download All Timesheets</h3>

		<form method='post' action='<?php echo admin_url( 'admin.php?page=timesheet'); ?>' id='payroll-report'>
			<table class="form-table">
				<tr>
					<th scope="row">Date Range:</th>
					<td>
						From <input type="text" size="10" name="thisdate" id="thisdate2" value="" /> to <input type="text" size="10" name="repeatuntil" id="repeatuntil2" value="" />
					</td>
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" name="submit" value="<?php _e( 'Print Timesheets', 'wpaesm' ); ?>" />
			</p>
		</form>

		<?php if( isset( $_POST['submit'] ) && 'View Timesheet' == $_POST['submit']) {
			if( ($_POST['thisdate'] == '____-__-__') || ($_POST['repeatuntil'] == '____-__-__')) {
				_e('You must enter both a start date and an end date to create a report.', 'wpaesm');
			} elseif($_POST['thisdate'] > $_POST['repeatuntil']) {
				_e('The report end date must be after the report begin date.', 'wpaesm');
			} else {
				$reportstart = $_POST['thisdate'];
				$reportend = $_POST['repeatuntil'];
				wpa_generate_admin_timesheet( $_POST['employee'], $reportstart, $reportend );
			} 
		} ?>

		<?php if( isset( $_POST['submit'] ) && 'Print Timesheets' == $_POST['submit']) {
			if( ($_POST['thisdate'] == '____-__-__') || ($_POST['repeatuntil'] == '____-__-__')) {
				_e('You must enter both a start date and an end date to create a report.', 'wpaesm');
			} elseif($_POST['thisdate'] > $_POST['repeatuntil']) {
				_e('The report end date must be after the report begin date.', 'wpaesm');
			} else {
				$reportstart = $_POST['thisdate'];
				$reportend = $_POST['repeatuntil'];
				wpa_download_timesheets( $reportstart, $reportend );
			}
		} ?>

	</div>
	<?php 
}

function wpa_download_timesheets() {

    // get all employees
    $employees = get_users( array( 'role' => 'employee' ) );
    $start = $_POST['thisdate'];
    $end = $_POST['repeatuntil'];

    echo '<div id="print-this">';
    echo '<a class="button" href="javascript:window.print()">Print</a>';

    // for each employee, generate a timesheet
    foreach( $employees as $employee ) {
        echo '<div class="timesheet">';
        wpa_generate_admin_timesheet( $employee->ID, $start, $end );
        echo '</div>';
    }

    echo '</div>';

}

function wpa_generate_employee_timesheet( $employee_id, $start, $end ) { 
	$employee = get_user_by( 'id', $employee_id ); 
	$nice_start = date( 'M d, Y', strtotime( $start ) );
	$nice_end = date( 'M d, Y', strtotime( $end ) ); ?>
	<h3><?php _e('Timesheet for ' . $employee->display_name . ', from ' . $nice_start . ' to ' . $nice_end, 'wpaesm'); ?></h3>

	<table id="payroll-report">
		<thead>
			<tr>
				<th>Day</th>
				<th>Shift 1</th>
				<th>Shift 2</th>
				<th>Shift 3</th>
				<th>Miles in personal vehicle</th>
				<th>Miles in company van (unpaid)</th>
				<th>Receipts</th>
				<th>Extra shift time</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			// set up some empty arrays so we can calculate totals later
			$tot_first_shift_hours = array();
			$tot_second_shift_hours = array();
			$tot_third_shift_hours = array();
			$tot_personal_miles = array();
			$tot_company_miles = array();
			$tot_receipts = array();
			$tot_extra_shifts = array();

			// get the days
			$days = wpa_get_list_of_days( $start, $end );
			foreach( $days as $day ) { ?>
				<tr>
					<th scope="row">
						<?php echo date( 'M d', strtotime( $day ) ); ?>
					</th>
					<td class="shift-1">
						<?php $first_shift = wpa_first_shift_info( $day, $employee_id ); ?>
						<?php if( isset( $first_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $first_shift['start'] ) );
						} ?>
						<?php if( isset( $first_shift['finish'] ) ) {
							echo " to " . date( "g:i a", strtotime( $first_shift['finish'] ) );
						} ?>
						<?php if( isset( $first_shift['duration'] ) ) {
							echo "<br /> ( " . $first_shift['duration'] . " hours )";
							$tot_first_shift_hours[] = $first_shift['duration'];
						} ?>
					</td>
					<td class="shift-2">
						<?php $second_shift = wpa_second_shift_info( $day, $employee_id, '1' ); ?>
						<?php if( isset( $second_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $second_shift['start'] ) );
						} ?>
						<?php if( isset( $second_shift['finish'] ) ) {
							echo " to " . date( "g:i a", strtotime( $second_shift['finish'] ) );
						} ?>
						<?php if( isset( $second_shift['duration'] ) ) {
							echo "<br /> ( " . $second_shift['duration'] . " hours )";
							$tot_second_shift_hours[] = $second_shift['duration'];
						} ?>
					</td>
					<td class="shift-3">
						<?php $third_shift = wpa_second_shift_info( $day, $employee_id, '2' ); ?>
						<?php if( isset( $third_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $third_shift['start'] ) );
						} ?>
						<?php if( isset( $third_shift['finish'] ) ) {
							echo " to " . date( "g:i a", strtotime( $third_shift['finish'] ) );
						} ?>
						<?php if( isset( $third_shift['duration'] ) ) {
							echo "<br /> ( " . $third_shift['duration'] . " hours )";
							$tot_third_shift_hours[] = $third_shift['duration'];
						} ?>
					</td>
					<td class="personal-miles">
						<?php 
						$cpv_miles = wpa_mileage_by_category( $day, $employee_id, array( 'personal-vehicle, personal-vehicle-second-shift', 'wrap-to-home' ) );
						echo $cpv_miles;
						$tot_cpv_miles[] = $cpv_miles; ?>
					</td>
					<td class="company-miles">
						<?php 
						$ccv_miles = wpa_mileage_by_category( $day, $employee_id, array( 'company-vehicle, company-vehicle-second-shift' ) );
						echo $ccv_miles;
						$tot_ccv_miles[] = $ccv_miles; ?>
					</td>
					<td>
						$<?php // receipts
						$receipts = wpa_calculate_receipts( $day, $employee_id );
						echo $receipts;
						$tot_receipts[] = $receipts; ?>
					</td>
					<td>
						<?php // extra shifts
						$extra_shift_time = wpa_extra_shift_info( $day, $employee_id, 'all' );
						echo $extra_shift_time;
						$tot_extra_shifts[] = $extra_shift_time; ?>
					</td>
				</tr>

			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th scope="row">
					Totals
				</th>
				<td class="total">
					<!-- total first shift hours -->
					<?php $total_first_shift_time = wpa_total_duration( $tot_first_shift_hours );
					echo $total_first_shift_time; ?>
				</td>
				<td class="total">
					<!-- total second shift hours -->
					<?php $total_second_shift_time = wpa_total_duration( $tot_second_shift_hours );
					echo $total_second_shift_time; ?>
				</td>
				<td class="total">
					<!-- total second shift hours -->
					<?php $total_third_shift_time = wpa_total_duration( $tot_third_shift_hours );
					echo $total_third_shift_time; ?>
				</td>
				<td class="total">
					<?php $total_personal_miles = array_sum( $tot_personal_miles ); 
					echo $total_personal_miles; ?>
				</td>
				<td class="total">
					<?php $total_company_miles = array_sum( $tot_company_miles ); 
					echo $total_company_miles; ?>
				</td>
				<td class="total">
					<!-- total receipts -->
					<?php $total_receipts = array_sum( $tot_receipts ); 
					echo '$'.$total_receipts; ?>
				</td>
				<td class="total">
					<!-- total extra shift hours -->
					<?php $total_extra_shift_time = wpa_total_duration( $tot_extra_shifts );
					echo $total_extra_shift_time; ?>
				</td>
			</tr>
			<tr>
				<td colspan="8">
					<b>Reg hours:</b> <?php echo wpaesm_reghours( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>PTO:</b> <?php echo wpaesm_ptohours( $employee_id, $start, $end, false ); ?> <br />
					<b>PTO 2:</b> <?php echo wpaesm_sicktime( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>OT:</b> <?php echo wpaesm_overtime( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>Total hours:</b> <?php echo wpaesm_totalhours( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>Miles:</b> <?php echo wpaesm_mileage( $employee_id, $start, $end ); ?> <br />
					<b>Receipts:</b> $<?php echo wpaesm_receipts( $employee_id, $start, $end ); ?>
				</td>
			</tr>
		</tfoot>
	</table>

<?php }

function wpa_generate_admin_timesheet( $employee_id, $start, $end ) { 
	$employee = get_user_by( 'id', $employee_id ); 
	$nice_start = date( 'M d, Y', strtotime( $start ) );
	$nice_end = date( 'M d, Y', strtotime( $end ) ); ?>
	<h3><?php _e('Timesheet for ' . $employee->display_name . ', from ' . $nice_start . ' to ' . $nice_end, 'wpaesm'); ?></h3>

	<table id="payroll-report">
		<thead>
			<tr>
				<th>Day</th>
				<th>Shift 1 start</th>
				<th>Shift 1 finish</th>
				<th>Shift 1 breaks</th>
				<th>Shift 1 hours worked</th>
				<th>Shift 2 start</th>
				<th>Shift 2 finish</th>
				<th>Shift 2 hours worked</th>
				<th>Shift 3 start</th>
				<th>Shift 3 finish</th>
				<th>Shift 3 hours worked</th>
				<th>Client miles in personal vehicle</th>
				<th>Client miles in company vehicle</th>
				<th>2nd-shift miles in personal vehicle</th>
				<th>2nd-shift miles in company vehicle</th>
				<th>Wrap to home miles</th>
				<th>Receipts</th>
				<th>Documentation Time</th>
				<th>Extra shift time</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			// set up some empty arrays so we can calculate totals later
			$tot_first_shift_hours = array();
			$tot_second_shift_hours = array();
			$tot_third_shift_hours = array();
			$tot_cpv_miles = array();
			$tot_ccv_miles = array();
			$tot_sspv_miles = array();
			$tot_sscv_miles = array();
			$tot_wth_miles = array();
			$tot_receipts = array();
			$tot_docs = array();
			$tot_extra_shifts = array();

			// get the days
			$days = wpa_get_list_of_days( $start, $end );
			foreach( $days as $day ) { ?>
				<tr>
					<th scope="row">
						<?php echo date( 'M d', strtotime( $day ) ); ?>
					</th>
					<?php $first_shift = wpa_first_shift_info( $day, $employee_id ); ?>
					<td>
						<?php if( isset( $first_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $first_shift['start'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $first_shift['finish'] ) ) {
							echo date( "g:i a", strtotime( $first_shift['finish'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $first_shift['break'] ) ) {
							echo $first_shift['break'];
						} ?>
					</td>
					<td>
						<?php if( isset( $first_shift['duration'] ) ) {
							echo $first_shift['duration'];
							$tot_first_shift_hours[] = $first_shift['duration'];
						} ?>
					</td>
					<?php $second_shift = wpa_second_shift_info( $day, $employee_id, '1' ); ?>
					<td>
						<?php if( isset( $second_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $second_shift['start'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $second_shift['finish'] ) ) {
							echo date( "g:i a", strtotime( $second_shift['finish'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $second_shift['duration'] ) ) {
							echo $second_shift['duration'];
							$tot_second_shift_hours[] = $second_shift['duration'];
						} ?>
					</td>
					<?php $third_shift = wpa_second_shift_info( $day, $employee_id, '2' ); ?>
					<td>
						<?php if( isset( $third_shift['start'] ) ) {
							echo date( "g:i a", strtotime( $third_shift['start'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $third_shift['finish'] ) ) {
							echo date( "g:i a", strtotime( $third_shift['finish'] ) );
						} ?>
					</td>
					<td>
						<?php if( isset( $third_shift['duration'] ) ) {
							echo $third_shift['duration'];
							$tot_third_shift_hours[] = $third_shift['duration'];
						} ?>
					</td>
					<td>
						<?php // miles in category mileage->client->personal-vehicle
						$cpv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle' );
						echo $cpv_miles;
						$tot_cpv_miles[] = $cpv_miles; ?>
					</td>
					<td>
						<?php // miles in category mileage->client->company-vehicle
						$ccv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle' );
						echo $ccv_miles;
						$tot_ccv_miles[] = $ccv_miles; ?>
					</td>
					<td>
						<?php // miles in 2nd-shift->personal
						$sspv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle-second-shift' ); 
						echo $sspv_miles;
						$tot_sspv_miles[] = $sspv_miles; ?>
					</td>
					<td>
						<?php // miles in 2nd-shift->company
						$sscv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle-second-shift' ); 
						echo $sscv_miles;
						$tot_sscv_miles[] = $sscv_miles; ?>
					</td>
					<td>
						<?php // miles in wrap-to-home
						$wth_miles = wpa_mileage_by_category( $day, $employee_id, 'wrap-to-home' ); 
						echo $wth_miles;
						$tot_wth_miles[] = $wth_miles; ?>
					</td>
					<td>
						$<?php // receipts
						$receipts = wpa_calculate_receipts( $day, $employee_id );
						echo $receipts;
						$tot_receipts[] = $receipts; ?>
					</td>
					<td>
						<?php // documentation
						$docs_time = wpa_extra_shift_info( $day, $employee_id, 'doc' );
						echo $docs_time;
						$tot_docs[] = $docs_time; ?>
					</td>
					<td>
						<?php // extra shifts
						$extra_shift_time = wpa_extra_shift_info( $day, $employee_id, 'extra' );
						echo $extra_shift_time;
						$tot_extra_shifts[] = $extra_shift_time; ?>
					</td>
				</tr>

			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th scope="row" colspan="4">
					Totals
				</th>
				<td class="total">
					<!-- total first shift hours -->
					<?php $total_first_shift_time = wpa_total_duration( $tot_first_shift_hours );
					echo $total_first_shift_time; ?>
				</td>
				<td>
					<!-- empty cell -->
				</td>
				<td>
					<!-- empty cell -->
				</td>
				<td class="total">
					<!-- total second shift hours -->
					<?php $total_second_shift_time = wpa_total_duration( $tot_second_shift_hours );
					echo $total_second_shift_time; ?>
				</td>
				<td>
					<!-- empty cell -->
				</td>
				<td>
					<!-- empty cell -->
				</td>
				<td class="total">
					<!-- total third shift hours -->
					<?php $total_third_shift_time = wpa_total_duration( $tot_third_shift_hours );
					echo $total_third_shift_time; ?>
				</td>
				<td class="total">
					<!-- total miles in mileage->client->personal -->
					<?php $total_cpv_miles = array_sum( $tot_cpv_miles ); 
					echo $total_cpv_miles; ?>
				</td>
				<td class="total">
					<!-- total miles in mileage->client->company -->
					<?php $total_ccv_miles = array_sum( $tot_ccv_miles ); 
					echo $total_ccv_miles; ?>
				</td>
				<td class="total">
					<!-- total miles in 2nd-shift->personal -->
					<?php $total_sspv_miles = array_sum( $tot_sspv_miles ); 
					echo $total_sspv_miles; ?>
				</td>
				<td class="total">
					<!-- total miles in 2nd-shift->company -->
					<?php $total_sscv_miles = array_sum( $tot_sscv_miles ); 
					echo $total_sscv_miles; ?>
				</td>
				<td class="total">
					<!-- total miles in wrap-to-home -->
					<?php $total_wth_miles = array_sum( $tot_wth_miles ); 
					echo $total_wth_miles; ?>
				</td>
				<td class="total">
					<!-- total receipts -->
					<?php $total_receipts = array_sum( $tot_receipts ); 
					echo '$'.$total_receipts; ?>
				</td>
				<td class="total-docs">
					<!-- total extra shift hours -->
					<?php $total_docs_time = wpa_total_duration( $tot_docs );
					echo $total_docs_time; ?>
				</td>
				<td class="total">
					<!-- total extra shift hours -->
					<?php $total_extra_shift_time = wpa_total_duration( $tot_extra_shifts );
					echo $total_extra_shift_time; ?>
				</td>
			</tr>
			<tr>
				<td colspan="19">
					<b>Reg hours:</b> <?php echo wpaesm_reghours( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>PTO:</b> <?php echo wpaesm_ptohours( $employee_id, $start, $end, false ); ?> <br />
					<b>PTO 2:</b> <?php echo wpaesm_sicktime( $employee_id, $start, $end ); ?> <br />
					<b>OT:</b> <?php echo wpaesm_overtime( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>Total hours:</b> <?php echo wpaesm_totalhours( $employee_id, $start, $end, 'scheduled-worked' ); ?> <br />
					<b>Miles:</b> <?php echo wpaesm_mileage( $employee_id, $start, $end ); ?> <br />
					<b>Receipts:</b> $<?php echo wpaesm_receipts( $employee_id, $start, $end ); ?>
				</td>
			</tr>
		</tfoot>
	</table>

<?php }

function wpa_get_list_of_days( $strDateFrom,$strDateTo ) {
    // http://stackoverflow.com/questions/4312439/php-return-all-dates-between-two-dates-in-an-array

    $days = array();

    $iDateFrom = strtotime( $strDateFrom );
    $iDateTo= strtotime( $strDateTo );

    if ($iDateTo>$iDateFrom)
    {
        array_push($days,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($days,date('Y-m-d',$iDateFrom));
        }
    }
    return array_unique( $days );
}

function wpa_first_shift_info( $day, $employee_id ) {
	$first_shift_info = '';

	// get the first shift of the day
	$args = array( 
		'post_type' => 'shift',
  	    'tax_query' => array(
		    'relation' => 'AND',
		      array(
		        'taxonomy' => 'shift_type',
		        'field' => 'slug',
		        'terms' => array( 'pto', 'respite-respite', 'extra' ),
		        'operator' => 'NOT IN'
		      ),
		      array(
		        'taxonomy' => 'shift_status',
		        'field' => 'slug',
		        'terms' => 'worked',
		        'operator' => 'IN'
		      )
	    ),
	    'posts_per_page' => 1,
	    'meta_query' => array(
			array(
			 'key' => '_wpaesm_date',
			 'value' => $day,
			 'type' => 'CHAR',
			 'compare' => '='
			),
	    ),
	    'meta_key' => '_wpaesm_starttime',
	    'orderby' => 'meta_value',
	    'order' => 'ASC',
	    'connected_type' => 'shifts_to_employees',
		'connected_items' => $employee_id,
	);

	$first_shift = new WP_Query( $args );

	if ( $first_shift->have_posts() ) {
		while ( $first_shift->have_posts() ) : $first_shift->the_post(); 
			global $shift_metabox;
			$meta = $shift_metabox->the_meta(); 
			// $first_shift_info['start'] = $meta['clockin'];
			// $first_shift_info['finish'] = $meta['clockout'];
			$first_shift_info['start'] = $meta['starttime'];
			$first_shift_info['finish'] = $meta['endtime'];
			if(isset( $meta['break'] ) && "1" == $meta['break'] ) {
				$first_shift_info['break'] =  "Yes"; 
			} 
			//$first_shift_info['duration'] = wpa_calculate_duration( $meta['clockin'], $meta['clockout'] );
			$first_shift_info['duration'] = wpa_calculate_duration( $meta['starttime'], $meta['endtime'] );
		endwhile;
	} 
	wp_reset_postdata();

	return $first_shift_info;
	
}

function wpa_second_shift_info( $day, $employee_id, $offset ) {
	$second_shift_data = array();

	// get the second shift of the day
	$args = array( 
		'post_type' => 'shift',
  	    'tax_query' => array(
		    'relation' => 'AND',
		      array(
		        'taxonomy' => 'shift_type',
		        'field' => 'slug',
		        'terms' => array( 'pto', 'respite-respite', 'extra' ),
		        'operator' => 'NOT IN'
		      ),
		      array(
		        'taxonomy' => 'shift_status',
		        'field' => 'slug',
		        'terms' => 'worked',
		        'operator' => 'IN'
		      )
	    ),
	    'posts_per_page' => 1,
	    'offset' => $offset,
	    'meta_key' => '_wpaesm_starttime',
	    'orderby' => 'meta_value',
	    'order' => 'ASC',
	    'meta_query' => array(
			array(
			 'key' => '_wpaesm_date',
			 'value' => $day,
			 'type' => 'CHAR',
			 'compare' => '='
			),
	    ),
	    'connected_type' => 'shifts_to_employees',
		'connected_items' => $employee_id,
	);

	$second_shift = new WP_Query( $args );


	if ( $second_shift->have_posts() ) {
		while ( $second_shift->have_posts() ) : $second_shift->the_post(); 
			global $shift_metabox;
			$meta = $shift_metabox->the_meta(); 
			// $second_shift_data['start'] = $meta['clockin'];
			// $second_shift_data['finish'] = $meta['clockout'];
			// $second_shift_data['duration'] = wpa_calculate_duration( $meta['clockin'], $meta['clockout'] );
			$second_shift_data['start'] = $meta['starttime'];
			$second_shift_data['finish'] = $meta['endtime'];
			$second_shift_data['duration'] = wpa_calculate_duration( $meta['starttime'], $meta['endtime'] );
		endwhile;
	} 
	wp_reset_postdata();

	return $second_shift_data;
	
}

function wpa_extra_shift_info( $day, $employee_id, $doc ) {
	$extra_shift = 0;
	$durations = array();

	if( 'doc' == $doc ) {
		$type = array(
		        'taxonomy' => 'shift_type',
		        'field' => 'slug',
		        'terms' => 'documentation',
		        'operator' => 'IN'
		      );
	} elseif( 'all' == $doc ) {
		$type = array(
		        'taxonomy' => 'shift_type',
		        'field' => 'slug',
		        'terms' => 'extra',
		        'operator' => 'IN'
		      );
	} else {
		$type = array(
				'relation' => 'AND',
				array( 
			        'taxonomy' => 'shift_type',
			        'field' => 'slug',
			        'terms' => 'extra',
			        'operator' => 'IN'
		        ),
		        array( 
			        'taxonomy' => 'shift_type',
			        'field' => 'slug',
			        'terms' => 'documentation',
			        'operator' => 'NOT IN'
		        )
		      );
	}

	// get the extra shifts
	$args = array( 
		'post_type' => 'shift',
  	    'tax_query' => array(
		    'relation' => 'AND',
		      array(
		        'taxonomy' => 'shift_status',
		        'field' => 'slug',
		        'terms' => 'worked',
		        'operator' => 'IN'
			  ),
		      $type,
	    ),
	    'posts_per_page' => -1,
	    'meta_query' => array(
			array(
			 'key' => '_wpaesm_date',
			 'value' => $day,
			 'type' => 'CHAR',
			 'compare' => '='
			),
	    ),
	    'connected_type' => 'shifts_to_employees',
		'connected_items' => $employee_id,
	);

	$extra_shifts = new WP_Query( $args );

	if ( $extra_shifts->have_posts() ) {
		while ( $extra_shifts->have_posts() ) : $extra_shifts->the_post(); 
			global $shift_metabox;
			$meta = $shift_metabox->the_meta(); 
			if( isset( $meta['starttime'] ) ) {
				$start = $meta['starttime'];
			} elseif( isset( $meta['clockin'] ) ) {
				$start = $meta['clockin'];
			} else {
				$start = 0;
			}
			if( isset( $meta['endtime'] ) ) {
				$end = $meta['endtime'];
			} elseif( isset( $meta['clockout'] ) ) {
				$end = $meta['clockout'];
			} else {
				$end = 0;
			}
			$durations[] = wpa_calculate_duration( $start, $end );
		endwhile;
	} 
	wp_reset_postdata();

	$extra_shift = wpa_total_duration( $durations );

	return $extra_shift;
	
}

function wpa_shift_notes( $day, $employee_id ) {
	$shift_notes = '';

	// get shifts with notes
	$args = array( 
		'post_type' => 'shift',
		'connected_type' => 'shifts_to_employees',
		'connected_items' => $employee_id,
  	    'tax_query' => array(
		    'relation' => 'AND',
		      array(
			        'taxonomy' => 'shift_status',
			        'field' => 'slug',
			        'terms' => 'worked',
			        'operator' => 'IN'
			  ),
	    ),
		array(
		 'key' => '_wpaesm_date',
		 'value' => $day,
		 'type' => 'CHAR',
		 'compare' => '='
		),
		array(
		 'key' => '_wpaesm_employeenote',
		 'value'   => array(''),
         'compare' => 'NOT IN'
		)
	);

	$shifts_with_notes = new WP_Query( $args );

	if ( $shifts_with_notes->have_posts() ) {
		while ( $shifts_with_notes->have_posts() ) : $shifts_with_notes->the_post(); 
			global $shift_metabox;
			$meta = $shift_metabox->the_meta(); 
			if( is_array( $meta['employeenote'] ) ) {
				foreach( $meta['employeenote'] as $note ) {
					$shift_notes .= '[' . $note['notedate'] . '] ' . $note['notetext'];
				}
			}
		endwhile;
	} 
	wp_reset_postdata();

	return $shift_notes;
	
}

function wpa_calculate_duration( $start, $end ) {
	$startTime=date_create(date("H:i",strtotime($start)));
	$endTime=date_create(date("H:i",strtotime($end)));
	$timeInterval=date_diff($startTime, $endTime);
	$getHours=$timeInterval->format('%h');
	$getMinutes=$timeInterval->format('%I');
	$duration=$getHours.":".$getMinutes;

	return $duration;
}

function wpa_total_duration( $durations ) {
	// http://stackoverflow.com/questions/2045807/php-add-up-a-series-of-minutesseconds
    $total = 0;
    foreach ($durations as $duration) {
        $duration = explode(':',$duration);
        $total += $duration[0] * 60;
        $total += $duration[1];
    }
    $mins = floor($total / 60);
    $secs = str_pad ( $total % 60, '2', '0', STR_PAD_LEFT);
    return $mins.':'.$secs;
}

function wpa_mileage_by_category( $day, $employee_id, $category_slug ) {
	$args = array( 
		'post_type' => 'expense',
  	    'tax_query' => array(
		      array(
			        'taxonomy' => 'expense_category',
			        'field' => 'slug',
			        'terms' => $category_slug,
			        'operator' => 'IN',
			        'include_children' => false
			  ),
	    ),
	    'posts_per_page' => -1,
	    'meta_query' => array(
			array(
			 'key' => '_wpaesm_date',
			 'value' => $day,
			 'type' => 'CHAR',
			 'compare' => '='
			),
	    ),
	    'connected_type' => 'expenses_to_employees',
		'connected_items' => $employee_id,
	);

	$mileage_query = new WP_Query( $args );

	$miles = 0;

	if ( $mileage_query->have_posts() ) {
		while ( $mileage_query->have_posts() ) : $mileage_query->the_post(); 
			global $expense_metabox;
			$meta = $expense_metabox->the_meta();
			$miles += $meta['amount'];
		endwhile;
	}

	return $miles;

}

function wpa_calculate_receipts( $day, $employee_id ) {
	$args = array( 
		'post_type' => 'expense',
  	    'tax_query' => array(
		      array(
			        'taxonomy' => 'expense_category',
			        'field' => 'slug',
			        'terms' => 'receipt',
			        'operator' => 'IN',
			        'include_children' => false
			  ),
	    ),
	    'posts_per_page' => -1,
	    'meta_query' => array(
			array(
			 'key' => '_wpaesm_date',
			 'value' => $day,
			 'type' => 'CHAR',
			 'compare' => '='
			),
	    ),
	    'connected_type' => 'expenses_to_employees',
		'connected_items' => $employee_id,
	);

	$receipts_query = new WP_Query( $args );

	$recipts = 0;

	if ( $receipts_query->have_posts() ) {
		while ( $receipts_query->have_posts() ) : $receipts_query->the_post(); 
			global $expense_metabox;
			$meta = $expense_metabox->the_meta();
			$recipts += $meta['amount'];
		endwhile;
	}

	return $recipts;	
}


function wpa_approve_timesheet() {

	if ( !wp_verify_nonce( $_POST['nonce'], "approve_timesheet_nonce" ) ) {
        die( "Permission error." );
    }

    $options = get_option('wpaesm_options');
    $employeeinfo = get_user_by( 'id', $_POST['employee'] );
    $employee_id = $_POST['employee'];

    $uploads = wp_upload_dir();
	$filename = $_POST['start']."-".$employeeinfo->user_login.".csv";
	$file = $uploads['basedir'] . "/timesheets/" . $filename;
	$file_link = $uploads['baseurl'] . "/timesheets/" . $filename;

	//open the file 
	$fh = fopen( $file, 'w' );

	$header_row = array(
		0 => 'Day',
		1 => 'Shift 1 start',
		2 => 'Shift 1 finish',
		3 => 'Shift 1 breaks',
		4 => 'Shift 1 hours worked',
		5 => 'Shift 2 start',
		6 => 'Shift 2 finish',
		7 => 'Shift 2 hours worked',
		8 => 'Shift 3 start',
		9 => 'Shift 3 finish',
		10 => 'Shift 3 hours worked',
		11 => 'Client miles in personal vehicle',
		12 => 'Client miles in company vehicle',
		13 => '2nd-shift miles in personal vehicle',
		14 => '2nd-shift miles in company vehicle',
		15 => 'Wrap to home miles',
		16 => 'Receipts',
		17 => 'Documentation time',
		18 => 'Extra shift time',
		19 => 'Notes',
	); 

	// set up some empty arrays so we can calculate totals later
	$tot_first_shift_hours = array();
	$tot_second_shift_hours = array();
	$tot_third_shift_hours = array();
	$tot_cpv_miles = array();
	$tot_ccv_miles = array();
	$tot_sspv_miles = array();
	$tot_sscv_miles = array();
	$tot_wth_miles = array();
	$tot_receipts = array();
	$tot_docs = array();
	$tot_extra_shifts = array();

	$data_rows = array();

	$days = wpa_get_list_of_days( $_POST['start'], $_POST['end'] );
	foreach( $days as $day ) {
		$row = array();
		$row[0] = $day;
		$first_shift = wpa_first_shift_info( $day, $_POST['employee'] );
		if( isset( $first_shift['start'] ) ) {
			$row[1] = date( "g:i a", strtotime( $first_shift['start'] ) );
		} else {
			$row[1] = '';
		}
		if( isset( $first_shift['finish'] ) ) {
			$row[2] = date( "g:i a", strtotime( $first_shift['finish'] ) );
		} else {
			$row[2] = '';
		}
		if( isset( $first_shift['break'] ) ) {
			$row[3] = $first_shift['break'];
		} else {
			$row[3] = '';
		}
		if( isset( $first_shift['duration'] ) ) {
			$row[4] =  $first_shift['duration'];
			$tot_first_shift_hours[] = $first_shift['duration'];
		} else {
			$row[4] = '';
		}
		$second_shift = wpa_second_shift_info( $day, $employee_id, 1 );
		if( isset( $second_shift['start'] ) ) {
			$row[5] = date( "g:i a", strtotime( $second_shift['start'] ) );
		} else {
			$row[5] = '';
		}
		if( isset( $second_shift['finish'] ) ) {
			$row[6] = date( "g:i a", strtotime( $second_shift['finish'] ) );
		} else {
			$row[6] = '';
		}
		if( isset( $second_shift['duration'] ) ) {
			$row[7] = $second_shift['duration'];
			$tot_second_shift_hours[] = $second_shift['duration'];
		} else {
			$row[7] = '';
		}
		$third_shift = wpa_second_shift_info( $day, $employee_id, 2 );
		if( isset( $third_shift['start'] ) ) {
			$row[5] = date( "g:i a", strtotime( $third_shift['start'] ) );
		} else {
			$row[5] = '';
		}
		if( isset( $third_shift['finish'] ) ) {
			$row[6] = date( "g:i a", strtotime( $third_shift['finish'] ) );
		} else {
			$row[6] = '';
		}
		if( isset( $third_shift['duration'] ) ) {
			$row[7] = $third_shift['duration'];
			$tot_third_shift_hours[] = $third_shift['duration'];
		} else {
			$row[7] = '';
		}
		$cpv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle' );
			$row[11] = $cpv_miles;
			$tot_cpv_miles[] = $cpv_miles; 
		$ccv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle' );
			$row[12] = $ccv_miles;
			$tot_ccv_miles[] = $ccv_miles;
		$sspv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle-second-shift' ); 
			$row[13] = $sspv_miles;
			$tot_sspv_miles[] = $sspv_miles;
		$sscv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle-second-shift' ); 
			$row[14] = $sscv_miles;
			$tot_sscv_miles[] = $sscv_miles;
		$wth_miles = wpa_mileage_by_category( $day, $employee_id, 'wrap-to-home' ); 
			$row[15] = $wth_miles;
			$tot_wth_miles[] = $wth_miles;
		$receipts = wpa_calculate_receipts( $day, $employee_id );
			$row[16] =  $receipts;
			$tot_receipts[] = $receipts;
		$docs_time = wpa_extra_shift_info( $day, $employee_id, 'doc' );
			$row[17] =  $docs_time;
			$tot_docs[] = $docs_time;
		$extra_shift_time = wpa_extra_shift_info( $day, $employee_id, 'extra' );
			$row[18] = $extra_shift_time;
			$tot_extra_shifts[] = $extra_shift_time;
		$row[19] = wpa_shift_notes( $day, $employee_id );

		$data_rows[] = $row;
	}
 
	$total_row = array(
		0 => 'Totals',
		1 => '',
		2 => '',
		3 => '',
		4 => array_sum( $tot_first_shift_hours ),
		5 => '',
		6 => '',
		7 => array_sum( $tot_second_shift_hours ),
		8 => '',
		9 => '',
		10 => array_sum( $tot_third_shift_hours ),
		11 => array_sum( $tot_cpv_miles ),
		12 => array_sum( $tot_ccv_miles ),
		13 => array_sum( $tot_sspv_miles ),
		14 => array_sum( $tot_sscv_miles ),
		15 => array_sum( $tot_wth_miles ),
		16 => array_sum( $tot_receipts ),
		17 => array_sum( $tot_docs ),
		18 => array_sum( $tot_extra_shifts ),
		19 => '',
		);

	$time_row = array( 
		0 => 'Time submitted: ' . date( 'Y-m-d g:ia' )
		);

	$data_rows[] = $total_row;
	$data_rows[] = $time_row;

	fputcsv( $fh, $header_row );
	foreach ( $data_rows as $data_row ) {
		fputcsv( $fh, $data_row );
	}
	fclose( $fh ); 

	// save timesheet in user meta
	$past_events = get_user_meta( $employee_id, 'timesheet_history', true );
	$this_event = array(
			'date' => date( 'Y-m-d', current_time( 'timestamp' ) ),
			'action' => 'approve',
			'file' => $file_link
		);
	if( !empty( $past_events ) ) {
		$past_events[] = $this_event;
	} else {
		$past_events = array(
			$this_event
			);
	}
	update_user_meta( $employee_id, 'timesheet_history', $past_events );
	$now = current_time( 'timestamp' );
	add_user_meta( $employee_id, 'last_timesheet', $now, true );

    // Send an email to site admins letting them know the timesheet has been approved
    $to = $options['admin_notification_email'];

    $subject = $employeeinfo->display_name . " has approved their timesheet for " . $_POST['start'] . ' to ' . $_POST['end'];

    $message = '<p>' . $employeeinfo->display_name . " has approved their timesheet for " . $_POST['start'] . ' to ' . $_POST['end'] . '</p>';
    $message .= '<p>The timesheet is attached.</p>';
    $message .= '<p><a href="' . $file_link . '">Download timesheet</a></p>';

    $from = "From: " . $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";

    $attachments = array( $file );

    wpaesm_send_email( $from, $to, '', $subject, $message, $attachments );

	$results = "<p class='success'>Your timesheet has been approved!</p>";
	die( $results );
}

add_action( 'wp_ajax_nopriv_wpa_approve_timesheet', 'wpa_approve_timesheet' ); // only need this on front end
add_action( 'wp_ajax_wpa_approve_timesheet', 'wpa_approve_timesheet' );

function wpa_disapprove_timesheet() {

	if ( !wp_verify_nonce( $_POST['nonce'], "disapprove_timesheet_nonce" ) ) {
        die( "Permission error." );
    }

    $options = get_option('wpaesm_options');
    $employeeinfo = get_user_by( 'id', $_POST['employee'] );
    $employee_id = $_POST['employee'];

    $uploads = wp_upload_dir();
	$filename = "disapproved-".$_POST['start']."-".$employeeinfo->user_login.".csv";
	$file = $uploads['basedir'] . "/timesheets/" . $filename;
	$file_link = $uploads['baseurl'] . "/timesheets/" . $filename;

	//open the file 
	$fh = fopen( $file, 'w' );

	$header_row = array(
		0 => 'Day',
		1 => 'Shift 1 start',
		2 => 'Shift 1 finish',
		3 => 'Shift 1 breaks',
		4 => 'Shift 1 hours worked',
		5 => 'Shift 2 start',
		6 => 'Shift 2 finish',
		7 => 'Shift 2 hours worked',
		8 => 'Shift 3 start',
		9 => 'Shift 3 finish',
		10 => 'Shift 3 hours worked',
		11 => 'Client miles in personal vehicle',
		12 => 'Client miles in company vehicle',
		13 => '2nd-shift miles in personal vehicle',
		14 => '2nd-shift miles in company vehicle',
		15 => 'Wrap to home miles',
		16 => 'Receipts',
		17 => 'Documentation time',
		18 => 'Extra shift time',
		19 => 'Notes',
	); 

	// set up some empty arrays so we can calculate totals later
	$tot_first_shift_hours = array();
	$tot_second_shift_hours = array();
	$tot_third_shift_hours = array();
	$tot_cpv_miles = array();
	$tot_ccv_miles = array();
	$tot_sspv_miles = array();
	$tot_sscv_miles = array();
	$tot_wth_miles = array();
	$tot_receipts = array();
	$tot_docs = array();
	$tot_extra_shifts = array();

	$data_rows = array();

	$days = wpa_get_list_of_days( $_POST['start'], $_POST['end'] );
	foreach( $days as $day ) {
		$row = array();
		$row[0] = $day;
		$first_shift = wpa_first_shift_info( $day, $_POST['employee'] );
		if( isset( $first_shift['start'] ) ) {
			$row[1] = date( "g:i a", strtotime( $first_shift['start'] ) );
		} else {
			$row[1] = '';
		}
		if( isset( $first_shift['finish'] ) ) {
			$row[2] = date( "g:i a", strtotime( $first_shift['finish'] ) );
		} else {
			$row[2] = '';
		}
		if( isset( $first_shift['break'] ) ) {
			$row[3] = $first_shift['break'];
		} else {
			$row[3] = '';
		}
		if( isset( $first_shift['duration'] ) ) {
			$row[4] =  $first_shift['duration'];
			$tot_first_shift_hours[] = $first_shift['duration'];
		} else {
			$row[4] = '';
		}
		$second_shift = wpa_second_shift_info( $day, $employee_id, 1 );
		if( isset( $second_shift['start'] ) ) {
			$row[5] = date( "g:i a", strtotime( $second_shift['start'] ) );
		} else {
			$row[5] = '';
		}
		if( isset( $second_shift['finish'] ) ) {
			$row[6] = date( "g:i a", strtotime( $second_shift['finish'] ) );
		} else {
			$row[6] = '';
		}
		if( isset( $second_shift['duration'] ) ) {
			$row[7] = $second_shift['duration'];
			$tot_second_shift_hours[] = $second_shift['duration'];
		} else {
			$row[7] = '';
		}
		$third_shift = wpa_second_shift_info( $day, $employee_id, 2 );
		if( isset( $third_shift['start'] ) ) {
			$row[5] = date( "g:i a", strtotime( $third_shift['start'] ) );
		} else {
			$row[5] = '';
		}
		if( isset( $third_shift['finish'] ) ) {
			$row[6] = date( "g:i a", strtotime( $third_shift['finish'] ) );
		} else {
			$row[6] = '';
		}
		if( isset( $third_shift['duration'] ) ) {
			$row[7] = $third_shift['duration'];
			$tot_third_shift_hours[] = $third_shift['duration'];
		} else {
			$row[7] = '';
		}
		$cpv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle' );
			$row[11] = $cpv_miles;
			$tot_cpv_miles[] = $cpv_miles; 
		$ccv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle' );
			$row[12] = $ccv_miles;
			$tot_ccv_miles[] = $ccv_miles;
		$sspv_miles = wpa_mileage_by_category( $day, $employee_id, 'personal-vehicle-second-shift' ); 
			$row[13] = $sspv_miles;
			$tot_sspv_miles[] = $sspv_miles;
		$sscv_miles = wpa_mileage_by_category( $day, $employee_id, 'company-vehicle-second-shift' ); 
			$row[14] = $sscv_miles;
			$tot_sscv_miles[] = $sscv_miles;
		$wth_miles = wpa_mileage_by_category( $day, $employee_id, 'wrap-to-home' ); 
			$row[15] = $wth_miles;
			$tot_wth_miles[] = $wth_miles;
		$receipts = wpa_calculate_receipts( $day, $employee_id );
			$row[16] =  $receipts;
			$tot_receipts[] = $receipts;
		$docs_time = wpa_extra_shift_info( $day, $employee_id, 'doc' );
			$row[17] =  $docs_time;
			$tot_docs[] = $docs_time;
		$extra_shift_time = wpa_extra_shift_info( $day, $employee_id, 'extra' );
			$row[18] = $extra_shift_time;
			$tot_extra_shifts[] = $extra_shift_time;
		$row[19] = wpa_shift_notes( $day, $employee_id );

		$data_rows[] = $row;
	}
 
	$total_row = array(
		0 => 'Totals',
		1 => '',
		2 => '',
		3 => '',
		4 => array_sum( $tot_first_shift_hours ),
		5 => '',
		6 => '',
		7 => array_sum( $tot_second_shift_hours ),
		8 => '',
		9 => '',
		10 => array_sum( $tot_third_shift_hours ),
		11 => array_sum( $tot_cpv_miles ),
		12 => array_sum( $tot_ccv_miles ),
		13 => array_sum( $tot_sspv_miles ),
		14 => array_sum( $tot_sscv_miles ),
		15 => array_sum( $tot_wth_miles ),
		16 => array_sum( $tot_receipts ),
		17 => array_sum( $tot_docs ),
		18 => array_sum( $tot_extra_shifts ),
		19 => '',
		);

	$time_row = array( 
		0 => 'Time submitted: ' . date( 'Y-m-d g:ia' )
		);

	$data_rows[] = $total_row;
	$data_rows[] = $time_row;

	fputcsv( $fh, $header_row );
	foreach ( $data_rows as $data_row ) {
		fputcsv( $fh, $data_row );
	}
	fclose( $fh ); 

	// save timesheet in user meta
	$past_events = get_user_meta( $employee_id, 'timesheet_history', true );
	$this_event = array(
			'date' => date( 'Y-m-d', current_time( 'timestamp' ) ),
			'action' => 'disapprove',
			'file' => $file_link
		);
	if( !empty( $past_events ) ) {
		$past_events[] = $this_event;
	} else {
		$past_events = array(
			$this_event
			);
	}
	update_user_meta( $employee_id, 'timesheet_history', $past_events );
	$now = current_time( 'timestamp' );
	add_user_meta( $employee_id, 'last_timesheet', $now, true );

    // Send an email to site admins letting them know the timesheet has been approved
    $to = $options['admin_notification_email'];

    $subject = $employeeinfo->display_name . " has requested changes to their timesheet for " . $_POST['start'] . ' to ' . $_POST['end'];

    $message = '<p>' . $employeeinfo->display_name . ' has requested changes to their timesheet for ' . $_POST['start'] . ' to ' . $_POST['end'] . '</p>';
    $message .= '<p><strong>Reason for disapproval: </strong><br />';
    $message .= $_POST['reasons'];
    $message .= '</p>';
    $message .= '<p>The timesheet is attached.</p>';
    $message .= '<p><a href="' . $file_link . '">Download timesheet</a></p>';

    $from = "From: " . $options['notification_from_name'] . "<" . $options['notification_from_email'] . ">";

    $attachments = array( $file );

    wpaesm_send_email( $from, $to, '', $subject, $message, $attachments );

	$results = "<p class='success'>Your request for changes has been sent.</p>";
	die( $results );
}

add_action( 'wp_ajax_nopriv_wpa_disapprove_timesheet', 'wpa_disapprove_timesheet' ); // only need this on front end
add_action( 'wp_ajax_wpa_disapprove_timesheet', 'wpa_disapprove_timesheet' );

// display timesheet history in user meta
add_action( 'show_user_profile', 'wpa_display_timesheet_history' );
add_action( 'edit_user_profile', 'wpa_display_timesheet_history' );

function wpa_display_timesheet_history( $user ) { ?>
    <h3>Timesheet History</h3>
   	<?php $history = get_user_meta( $user->ID, 'timesheet_history', true ); 
   	if( !empty( $history ) ) { ?>
   		<table class="wp-list-table widefat fixed posts striped">
   			<thead>
   				<tr>
   					<th>Date</th>
   					<th>Action</th>
   					<th>Timesheet</th>
   				</tr>
   			</thead>
   			<tbody>
		   		<?php foreach( $history as $event ) { ?>
		   			<tr>	
		   				<td><?php echo $event['date']; ?></td>
		   				<td><?php echo $event['action']; ?></td>
		   				<td><a href="<?php echo $event['file']; ?>" target="_blank">Download</a></td>
		   			</tr>
		   		<?php } ?>
		   	</tbody>
   		</table>
   	<?php } else { ?>
   		<p>There is no timesheet history for this employee.</p>
   	<?php } ?>


    <?php
}

// Clock-in/out report

add_action('admin_menu', 'wpa_add_clock_in_out_page');
function wpa_add_clock_in_out_page() {
	add_submenu_page( '/employee-schedule-manager/options.php', 'Clock In/Out', 'Clock In/Out', 'manage_options', 'clock-in-out', 'wpa_clock_in_out_report_page' );
}

function wpa_add_scripts_to_clock_in_out($hook)
{
	if ( 'employee-schedule-manager_page_clock-in-out' == $hook ) {
		wp_enqueue_script( 'date-time-picker', plugins_url() . '/employee-schedule-manager/js/jquery.datetimepicker.js', 'jQuery' );
		wp_enqueue_script( 'wpaesm_scripts', plugins_url() . '/employee-schedule-manager/js/wpaesmscripts.js', 'jQuery' );
		wp_enqueue_script( 'validate', plugins_url() . '/employee-schedule-manager/js/jquery.validate.min.js', 'jQuery' );
		wp_enqueue_script( 'stupid-table', plugins_url() . '/employee-schedule-manager/js/stupidtable.min.js', array( 'jquery' ) );
	}
}
add_action( 'admin_enqueue_scripts', 'wpa_add_scripts_to_clock_in_out' );

function wpa_clock_in_out_report_page() { ?>
	<div class="wrap">
		
		<!-- Display Plugin Icon, Header, and Description -->
		<div class="icon32" id="icon-options-general"><br></div>
		<h2><?php _e('Clock In/Out Report', 'wpaesm'); ?></h2>

		
		<form method='post' action='<?php echo admin_url( 'admin.php?page=clock-in-out'); ?>' id='clock-in-out-report'>
			<table class="form-table">
				<tr>
					<th scope="row"><?php _e( 'Date Range: ', 'wpaesm' ); ?></th>
					<td>
						<?php _e( 'From ', 'wpaesm' ); ?>
						<input type="text" size="10" name="thisdate" id="thisdate" value="" /> 
						<?php _e( ' to ', 'wpaesm'); ?> 
						<input type="text" size="10" name="repeatuntil" id="repeatuntil" value="" />
					</td>					
				</tr>
			</table>
			<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Generate Report', 'wpaesm' ); ?>" />
			</p>
		</form>

		<?php if( $_POST ) { 
			if( ( $_POST['thisdate'] == '____-__-__') || ( $_POST['repeatuntil'] == '____-__-__' ) ) {
				_e('You must enter both a start date and an end date to create a report.', 'wpaesm');
			} elseif( $_POST['thisdate'] > $_POST['repeatuntil'] ) {
				_e( 'The report end date must be after the report begin date.', 'wpaesm' );
			} else {
				// find all of the shifts
				// can they be ordered by employee?  probably need js for that
				// for each shift, make a row showing shift name, scheduled start/end, actual start/end, difference
				$args = array(
					'post_type' => 'shift',
					'posts_per_page' => -1,
					'meta_query' => array(
						array(
							'key' => '_wpaesm_date',
					         'value' => $_POST['thisdate'],
					         'type' => 'CHAR',
					         'compare' => '>='
							),
						array(
							'key' => '_wpaesm_date',
					         'value' => $_POST['repeatuntil'],
					         'type' => 'CHAR',
					         'compare' => '<='
							)
						),
					'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'shift_status',
						        'field' => 'slug',
						        'terms' => 'worked',
						        'operator' => 'IN'
							),
							array(
								'taxonomy' => 'shift_type',
						        'field' => 'slug',
						        'terms' => 'extra',
						        'operator' => 'NOT IN'
							)
						)
					);

			$worked = new WP_Query( $args );
			
			// The Loop
			if ( $worked->have_posts() ) { ?>
				<table id="filtered-shifts" class="wp-list-table widefat fixed posts">
					<thead>
						<tr>
							<th data-sort='string'><span><?php _e( 'Employee', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Date', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Shift', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Time In', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Time Out', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Location In', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Location Out', 'wpaesm' ); ?></span></th>
							<th data-sort='string'><span><?php _e( 'Notes', 'wpaesm' ); ?></span></th>
						</tr>
					</thead>
					<tbody>
						<?php while ( $worked->have_posts() ) : $worked->the_post(); 
							$postid = get_the_id();
							global $shift_metabox;
							$meta = $shift_metabox->the_meta(); 
							// get employee associated with this shift
							$users = get_users( array(
								'connected_type' => 'shifts_to_employees',
								'connected_items' => $postid,
							) );
							if( isset( $users ) ) {
								foreach( $users as $user ) {
									$employee = $user->display_name;
									$employeeid = $user->ID;
								}
							}?>
							<tr>	
								<td class="employee">
									<?php if( isset( $employeeid ) ) { ?>
										<a href="<?php echo get_edit_user_link( $employeeid ); ?>"><?php echo $employee; ?></a>
										<?php unset( $employeeid);
									} ?>
								</td>
								<td class="date">
									<?php if( isset( $meta['date'] ) ) {
										echo $meta['date'];
									} ?>
								</td>
								<td class="shift">	
									<?php the_title(); ?><br />
									<a href="<?php echo get_edit_post_link(); ?>"><?php _e( 'Edit', 'wpaesm' ); ?></a> |
									<a href="<?php the_permalink(); ?>"><?php _e( 'View', 'wpaesm' ); ?></a>
								</td>
								<td class="time-in">
									<?php if( isset( $meta['clockin'] ) && '' !== $meta['clockin'] ) {
										echo $meta['clockin'];
									} ?>
								</td>
								<td class="time-out">
									<?php if( isset( $meta['clockout'] ) && '' !== $meta['clockout'] ) {
										echo $meta['clockout'];
									} ?>
								</td>
								<td class="location-in">
									<?php if( isset( $meta['location_in'] ) && '' !== $meta['location_in'] ) {
										echo $meta['location_in'];
									} ?>
								</td>
								<td class="location-out">
									<?php if( isset( $meta['location_out'] ) && '' !== $meta['location_out'] ) {
										echo $meta['location_out'];
									} ?>
								</td>
								<td class="notes">
									<?php if( isset( $meta['employeenote'] ) && is_array( $meta['employeenote'] ) ) { ?>
										<ul>
											<?php foreach( $meta['employeenote'] as $note ) { ?>
												<li><strong><?php echo $note['notedate']; ?>: </strong> <?php echo $note['notetext']; ?></li>
											<?php } ?>
										</ul>
									<?php } ?>
								</td>
							</tr>

						<?php endwhile; ?>
					</tbody>
				</table>
			<?php } else {
				echo '<p>' . __( 'Sorry, no shifts were found', 'wpaesm' ) . '</p>';
				}
			}
		} ?>


	</div>
<?php }

// Add link to master schedule to toolbar
add_action( 'admin_bar_menu', 'wpaesm_master_schedule_link', 999 );

function wpaesm_master_schedule_link( $wp_admin_bar ) {
	$args = array(
		'id'    => 'master_schedule',
		'title' => 'Master Schedule',
		'href'  => 'http://seattlecommunitycare.com/your-profile/master-schedule-2/',
		'meta'  => array( 'class' => 'my-toolbar-page' )
	);
	$wp_admin_bar->add_node( $args );
}

?>