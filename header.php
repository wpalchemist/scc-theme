<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title(); ?></title>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
     
    <script src="<?php bloginfo('template_directory'); ?>/js/modernizr-1.6.min.js"></script>
     
    <?php wp_head(); ?>
</head>
 
<body <?php body_class(); ?>>

<div id="wrapper">
 
    <header id="top">
        <div class="logo">
            <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <h1><?php bloginfo( 'name' ); ?></h1>
                <p class="description"><?php bloginfo( 'description' ); ?></p>
            </a>
        </div>
        <a class="offscreen" href="#content">Skip to main content</a>
        <?php wp_nav_menu( array( 'container' => 'nav', 'fallback_cb' => 'wpa_menu', 'theme_location' => 'primary' ) ); ?>
        <?php echo do_shortcode('[wppb-logout]'); ?>
        <?php if( is_user_logged_in() ) { ?>
            <span class="edit-profile">
                <a href="<?php echo home_url(); ?>/your-profile">Employee Home | </a>
                <a href="<?php echo home_url(); ?>/your-profile/edit-your-account">Account | </a>
            </span>
        <?php } ?>
    </header>

    <div id="content" class="clearfix">
        <div id="main">