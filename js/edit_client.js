jQuery(document).ready(function($){
      var ed = new tinymce.Editor('editorid', {
         // some_setting : 1
      });

      var eda = new tinymce.Editor('editorida', {
         // some_setting : 1
      });

      var edb = new tinymce.Editor('editoridb', {
         // some_setting : 1
      });


      var edc = new tinymce.Editor('editoridc', {
         // some_setting : 1
      });


      var edd = new tinymce.Editor('editoridd', {
         // some_setting : 1
      });


      var ede = new tinymce.Editor('editoride', {
         // some_setting : 1
      });


     // ed.render();

     $('.editable-description').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: ed,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var description = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'description': description,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });

     $('.editable-diagnosis').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: eda,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var diagnosis = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'diagnosis': diagnosis,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });

     $('.editable-triggers').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: edb,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var triggers = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'triggers': triggers,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });

     $('.editable-activities').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: edc,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var activities = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'activities': activities,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });

     $('.editable-sub').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: edd,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var sub = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'sub': sub,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });

     $('.editable-crisis').editable(
          {
         type: 'wysiwyg',  // other options: text
         editor: ede,
         onSubmit:function submitData(content){
            var url = editclientajax.ajaxurl;
            var crisis = content;
            var clientid = $('#clientid').val();
            var nonce = $('#wpascc_client_nonce').val();
            var data = {
                'action': 'wpascc_save_client_description',
                'crisis': crisis,
                'clientid': clientid,
                'nonce': nonce,
            };
            // update the subtotal, then add it to the donation to get the total
            $.post(url, data, function (response) {
                console.log(response);
            });
         },
         submit:'save',
         cancel:'cancel'
     });



$("#subTag").on("click", function(){
     $('#sub').toggle();
     if ($('#sub').css('display') == 'none') {
          $('#subTag').html('Show Sub Plan');
     }
     else {
          $('#subTag').html('Hide Sub Plan');
     }
});

$("#crisisTag").on("click", function(){
     $('#crisis').toggle();
     if ($('#crisis').css('display') == 'none') {
          $('#crisisTag').html('Show Crisis Plan');
     }
     else {
          $('#crisisTag').html('Hide Crisis Plan');
     }
});


}); // end anonymous function